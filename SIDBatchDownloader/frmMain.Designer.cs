﻿namespace SIDBatchDownloader
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            ExtendedWinControl.GridFilterFactories.DefaultGridFilterFactory defaultGridFilterFactory1 = new ExtendedWinControl.GridFilterFactories.DefaultGridFilterFactory();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.prevTab = new System.Windows.Forms.TabControl();
            this.tpMain = new System.Windows.Forms.TabPage();
            this.cbPrcAll = new System.Windows.Forms.CheckBox();
            this.lblBatchID = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblCurrStage = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.tpPreview = new System.Windows.Forms.TabPage();
            this.wbIE = new System.Windows.Forms.WebBrowser();
            this.tpRename = new System.Windows.Forms.TabPage();
            this.btnUpIDI = new System.Windows.Forms.Button();
            this.btnRename = new System.Windows.Forms.Button();
            this.btnIDIDir = new System.Windows.Forms.Button();
            this.btnTxtDir = new System.Windows.Forms.Button();
            this.txtIDIDir = new System.Windows.Forms.TextBox();
            this.txtTxtDir = new System.Windows.Forms.TextBox();
            this.lblIDIDir = new System.Windows.Forms.Label();
            this.lblTxtDir = new System.Windows.Forms.Label();
            this.tFailedRename = new System.Windows.Forms.TextBox();
            this.tpInq = new System.Windows.Forms.TabPage();
            this.lblAppCount = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvInquiry = new System.Windows.Forms.DataGridView();
            this.btnInqExport = new System.Windows.Forms.Button();
            this.btnInqBack = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtInqName = new System.Windows.Forms.TextBox();
            this.lblInqName = new System.Windows.Forms.Label();
            this.gbFilterInq = new System.Windows.Forms.GroupBox();
            this.lblFilterName = new System.Windows.Forms.Label();
            this.lblFilterRegno = new System.Windows.Forms.Label();
            this.txtFilterName = new System.Windows.Forms.TextBox();
            this.txtFilterRegno = new System.Windows.Forms.TextBox();
            this.txtInqRegno = new System.Windows.Forms.TextBox();
            this.lblInqRegno = new System.Windows.Forms.Label();
            this.txtInqTxtName = new System.Windows.Forms.TextBox();
            this.lblInqTxtName = new System.Windows.Forms.Label();
            this.dtpInqTo = new System.Windows.Forms.DateTimePicker();
            this.dtpInqFrom = new System.Windows.Forms.DateTimePicker();
            this.btnInqSearch = new System.Windows.Forms.Button();
            this.cmbInqStatus = new System.Windows.Forms.ComboBox();
            this.txtInqBatName = new System.Windows.Forms.TextBox();
            this.lblInqTo = new System.Windows.Forms.Label();
            this.lblInqDate = new System.Windows.Forms.Label();
            this.lblInqBatName = new System.Windows.Forms.Label();
            this.lblInqStatus = new System.Windows.Forms.Label();
            this.tpSetting = new System.Windows.Forms.TabPage();
            this.gbWebIDB = new System.Windows.Forms.GroupBox();
            this.nMaxPageIDB = new System.Windows.Forms.NumericUpDown();
            this.lblMaxLoopIDB = new System.Windows.Forms.Label();
            this.nMaxLoopIDB = new System.Windows.Forms.NumericUpDown();
            this.lblMaxPageIDB = new System.Windows.Forms.Label();
            this.gbWebBKM = new System.Windows.Forms.GroupBox();
            this.nMaxPageBKM = new System.Windows.Forms.NumericUpDown();
            this.lblMaxLoopBKM = new System.Windows.Forms.Label();
            this.nMaxLoopBKM = new System.Windows.Forms.NumericUpDown();
            this.lblMaxPageBKM = new System.Windows.Forms.Label();
            this.gbMisc = new System.Windows.Forms.GroupBox();
            this.nMinDataGen = new System.Windows.Forms.NumericUpDown();
            this.cbAutoConvert = new System.Windows.Forms.CheckBox();
            this.nLoop = new System.Windows.Forms.NumericUpDown();
            this.lblLoop = new System.Windows.Forms.Label();
            this.nDelay = new System.Windows.Forms.NumericUpDown();
            this.lblDelayTime = new System.Windows.Forms.Label();
            this.nMaxTxtGen = new System.Windows.Forms.NumericUpDown();
            this.lblDtGen = new System.Windows.Forms.Label();
            this.lblTxtGen = new System.Windows.Forms.Label();
            this.nMaxDataGen = new System.Windows.Forms.NumericUpDown();
            this.cbResume = new System.Windows.Forms.CheckBox();
            this.gbTime = new System.Windows.Forms.GroupBox();
            this.lblFreqEvery = new System.Windows.Forms.Label();
            this.lblFreqMinute = new System.Windows.Forms.Label();
            this.nFreqMinute = new System.Windows.Forms.NumericUpDown();
            this.lblsd = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblDays = new System.Windows.Forms.Label();
            this.ccbDays = new ExtendedWinControl.CheckedComboBox();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.gbUserWeb = new System.Windows.Forms.GroupBox();
            this.txtKdCbgWeb = new System.Windows.Forms.TextBox();
            this.txtKdBankWeb = new System.Windows.Forms.TextBox();
            this.lblKodeCabangWeb = new System.Windows.Forms.Label();
            this.lblKodeBankWeb = new System.Windows.Forms.Label();
            this.cbEqBatch = new System.Windows.Forms.CheckBox();
            this.txtPwdWeb = new System.Windows.Forms.TextBox();
            this.txtUserWeb = new System.Windows.Forms.TextBox();
            this.lblPwdWeb = new System.Windows.Forms.Label();
            this.lblUserWeb = new System.Windows.Forms.Label();
            this.gbSearchCrit = new System.Windows.Forms.GroupBox();
            this.txtInitialLetter = new System.Windows.Forms.TextBox();
            this.lblInitialLetter = new System.Windows.Forms.Label();
            this.txtUIDCBS = new System.Windows.Forms.TextBox();
            this.cbUseCuType = new System.Windows.Forms.CheckBox();
            this.cbLOSInt = new System.Windows.Forms.CheckBox();
            this.lblModule = new System.Windows.Forms.Label();
            this.cbPL = new System.Windows.Forms.CheckBox();
            this.cbCritNPWP = new System.Windows.Forms.CheckBox();
            this.cbCC = new System.Windows.Forms.CheckBox();
            this.cbCritKTP = new System.Windows.Forms.CheckBox();
            this.cbCritGender = new System.Windows.Forms.CheckBox();
            this.cbCritDOB = new System.Windows.Forms.CheckBox();
            this.cbCritName = new System.Windows.Forms.CheckBox();
            this.gbFolder = new System.Windows.Forms.GroupBox();
            this.btnBKM = new System.Windows.Forms.Button();
            this.txtBKMFolder = new System.Windows.Forms.TextBox();
            this.btnOutput = new System.Windows.Forms.Button();
            this.btnInput = new System.Windows.Forms.Button();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.txtInputFolder = new System.Windows.Forms.TextBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.chkDwn = new System.Windows.Forms.CheckBox();
            this.chkAppr = new System.Windows.Forms.CheckBox();
            this.chkReq = new System.Windows.Forms.CheckBox();
            this.gbUserApr = new System.Windows.Forms.GroupBox();
            this.txtKdCbgApr = new System.Windows.Forms.TextBox();
            this.txtKdBankApr = new System.Windows.Forms.TextBox();
            this.lblKodeCabangApr = new System.Windows.Forms.Label();
            this.lblKodeBankApr = new System.Windows.Forms.Label();
            this.lblPwdApr = new System.Windows.Forms.Label();
            this.txtPwdAppr = new System.Windows.Forms.TextBox();
            this.txtUserAppr = new System.Windows.Forms.TextBox();
            this.lblUserApr = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbConn = new System.Windows.Forms.GroupBox();
            this.lblDBName = new System.Windows.Forms.Label();
            this.txtDBName = new System.Windows.Forms.TextBox();
            this.lblDBPwd = new System.Windows.Forms.Label();
            this.lblDBUser = new System.Windows.Forms.Label();
            this.lblDBHost = new System.Windows.Forms.Label();
            this.txtDBPwd = new System.Windows.Forms.TextBox();
            this.txtDBUid = new System.Windows.Forms.TextBox();
            this.txtDBIP = new System.Windows.Forms.TextBox();
            this.gbUserReq = new System.Windows.Forms.GroupBox();
            this.txtKdCbgReq = new System.Windows.Forms.TextBox();
            this.txtKdBankReq = new System.Windows.Forms.TextBox();
            this.lblKodeCabangReq = new System.Windows.Forms.Label();
            this.lblKodeBankReq = new System.Windows.Forms.Label();
            this.lblPwdReq = new System.Windows.Forms.Label();
            this.txtPwdReq = new System.Windows.Forms.TextBox();
            this.txtUserReq = new System.Windows.Forms.TextBox();
            this.lblUserReq = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.OpenDirBatch = new System.Windows.Forms.FolderBrowserDialog();
            this.tmrRealTime = new System.Windows.Forms.Timer(this.components);
            this.lblCurrProgress = new System.Windows.Forms.Label();
            this.pbCurr = new System.Windows.Forms.ProgressBar();
            this.lblAddProgress = new System.Windows.Forms.Label();
            this._extender = new ExtendedWinControl.DataGridFilterExtender(this.components);
            this.dlgSaveExport = new System.Windows.Forms.SaveFileDialog();
            this.cbMF = new System.Windows.Forms.CheckBox();
            this.cbCO = new System.Windows.Forms.CheckBox();
            this.prevTab.SuspendLayout();
            this.tpMain.SuspendLayout();
            this.tpPreview.SuspendLayout();
            this.tpRename.SuspendLayout();
            this.tpInq.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInquiry)).BeginInit();
            this.panel1.SuspendLayout();
            this.gbFilterInq.SuspendLayout();
            this.tpSetting.SuspendLayout();
            this.gbWebIDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxPageIDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxLoopIDB)).BeginInit();
            this.gbWebBKM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxPageBKM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxLoopBKM)).BeginInit();
            this.gbMisc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMinDataGen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nLoop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxTxtGen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDataGen)).BeginInit();
            this.gbTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nFreqMinute)).BeginInit();
            this.gbUserWeb.SuspendLayout();
            this.gbSearchCrit.SuspendLayout();
            this.gbFolder.SuspendLayout();
            this.gbUserApr.SuspendLayout();
            this.gbConn.SuspendLayout();
            this.gbUserReq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._extender)).BeginInit();
            this.SuspendLayout();
            // 
            // prevTab
            // 
            this.prevTab.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.prevTab.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.prevTab.Controls.Add(this.tpMain);
            this.prevTab.Controls.Add(this.tpPreview);
            this.prevTab.Controls.Add(this.tpRename);
            this.prevTab.Controls.Add(this.tpInq);
            this.prevTab.Controls.Add(this.tpSetting);
            this.prevTab.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.prevTab.ItemSize = new System.Drawing.Size(20, 100);
            this.prevTab.Location = new System.Drawing.Point(6, 6);
            this.prevTab.Multiline = true;
            this.prevTab.Name = "prevTab";
            this.prevTab.SelectedIndex = 0;
            this.prevTab.Size = new System.Drawing.Size(790, 431);
            this.prevTab.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.prevTab.TabIndex = 0;
            this.prevTab.Selected += new System.Windows.Forms.TabControlEventHandler(this.prevTab_Selected);
            this.prevTab.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.prevTab_DrawItem);
            // 
            // tpMain
            // 
            this.tpMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.tpMain.Controls.Add(this.cbPrcAll);
            this.tpMain.Controls.Add(this.lblBatchID);
            this.tpMain.Controls.Add(this.btnReset);
            this.tpMain.Controls.Add(this.lblCurrStage);
            this.tpMain.Controls.Add(this.btnStart);
            this.tpMain.Controls.Add(this.btnStop);
            this.tpMain.Location = new System.Drawing.Point(116, 4);
            this.tpMain.Name = "tpMain";
            this.tpMain.Padding = new System.Windows.Forms.Padding(3);
            this.tpMain.Size = new System.Drawing.Size(670, 412);
            this.tpMain.TabIndex = 0;
            this.tpMain.Text = "Main";
            this.tpMain.UseVisualStyleBackColor = true;
            // 
            // cbPrcAll
            // 
            this.cbPrcAll.AutoSize = true;
            this.cbPrcAll.Location = new System.Drawing.Point(23, 358);
            this.cbPrcAll.Name = "cbPrcAll";
            this.cbPrcAll.Size = new System.Drawing.Size(78, 17);
            this.cbPrcAll.TabIndex = 6;
            this.cbPrcAll.Text = "Process All";
            this.cbPrcAll.UseVisualStyleBackColor = true;
            // 
            // lblBatchID
            // 
            this.lblBatchID.AutoSize = true;
            this.lblBatchID.Location = new System.Drawing.Point(20, 68);
            this.lblBatchID.Name = "lblBatchID";
            this.lblBatchID.Size = new System.Drawing.Size(0, 13);
            this.lblBatchID.TabIndex = 5;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(575, 383);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 4;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblCurrStage
            // 
            this.lblCurrStage.AutoSize = true;
            this.lblCurrStage.Location = new System.Drawing.Point(20, 28);
            this.lblCurrStage.Name = "lblCurrStage";
            this.lblCurrStage.Size = new System.Drawing.Size(0, 13);
            this.lblCurrStage.TabIndex = 3;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnStart.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.White;
            this.btnStart.Location = new System.Drawing.Point(242, 353);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(64, 26);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Crimson;
            this.btnStop.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnStop.FlatAppearance.BorderSize = 4;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnStop.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.Color.White;
            this.btnStop.Location = new System.Drawing.Point(364, 353);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(64, 26);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // tpPreview
            // 
            this.tpPreview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.tpPreview.Controls.Add(this.wbIE);
            this.tpPreview.Location = new System.Drawing.Point(116, 4);
            this.tpPreview.Name = "tpPreview";
            this.tpPreview.Padding = new System.Windows.Forms.Padding(3);
            this.tpPreview.Size = new System.Drawing.Size(670, 412);
            this.tpPreview.TabIndex = 2;
            this.tpPreview.Text = "IE Preview";
            this.tpPreview.UseVisualStyleBackColor = true;
            // 
            // wbIE
            // 
            this.wbIE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbIE.Location = new System.Drawing.Point(3, 3);
            this.wbIE.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbIE.Name = "wbIE";
            this.wbIE.ScriptErrorsSuppressed = true;
            this.wbIE.Size = new System.Drawing.Size(664, 406);
            this.wbIE.TabIndex = 0;
            this.wbIE.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.wbIE_Navigating);
            this.wbIE.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.wbIE_DocumentCompleted);
            this.wbIE.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.wbIE_Navigated);
            // 
            // tpRename
            // 
            this.tpRename.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.tpRename.Controls.Add(this.btnUpIDI);
            this.tpRename.Controls.Add(this.btnRename);
            this.tpRename.Controls.Add(this.btnIDIDir);
            this.tpRename.Controls.Add(this.btnTxtDir);
            this.tpRename.Controls.Add(this.txtIDIDir);
            this.tpRename.Controls.Add(this.txtTxtDir);
            this.tpRename.Controls.Add(this.lblIDIDir);
            this.tpRename.Controls.Add(this.lblTxtDir);
            this.tpRename.Controls.Add(this.tFailedRename);
            this.tpRename.Location = new System.Drawing.Point(116, 4);
            this.tpRename.Name = "tpRename";
            this.tpRename.Padding = new System.Windows.Forms.Padding(3);
            this.tpRename.Size = new System.Drawing.Size(670, 412);
            this.tpRename.TabIndex = 4;
            this.tpRename.Text = "Converter";
            this.tpRename.UseVisualStyleBackColor = true;
            // 
            // btnUpIDI
            // 
            this.btnUpIDI.Enabled = false;
            this.btnUpIDI.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpIDI.Location = new System.Drawing.Point(571, 36);
            this.btnUpIDI.Name = "btnUpIDI";
            this.btnUpIDI.Size = new System.Drawing.Size(58, 23);
            this.btnUpIDI.TabIndex = 12;
            this.btnUpIDI.Text = "Upload!";
            this.btnUpIDI.UseVisualStyleBackColor = true;
            this.btnUpIDI.Click += new System.EventHandler(this.btnUpIDI_Click);
            // 
            // btnRename
            // 
            this.btnRename.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRename.Location = new System.Drawing.Point(571, 6);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(58, 23);
            this.btnRename.TabIndex = 11;
            this.btnRename.Text = "Rename!";
            this.btnRename.UseVisualStyleBackColor = true;
            this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
            // 
            // btnIDIDir
            // 
            this.btnIDIDir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnIDIDir.Location = new System.Drawing.Point(518, 20);
            this.btnIDIDir.Name = "btnIDIDir";
            this.btnIDIDir.Size = new System.Drawing.Size(23, 23);
            this.btnIDIDir.TabIndex = 10;
            this.btnIDIDir.Text = "...";
            this.btnIDIDir.UseVisualStyleBackColor = true;
            this.btnIDIDir.Click += new System.EventHandler(this.btnIDIDir_Click);
            // 
            // btnTxtDir
            // 
            this.btnTxtDir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTxtDir.Location = new System.Drawing.Point(243, 20);
            this.btnTxtDir.Name = "btnTxtDir";
            this.btnTxtDir.Size = new System.Drawing.Size(23, 23);
            this.btnTxtDir.TabIndex = 9;
            this.btnTxtDir.Text = "...";
            this.btnTxtDir.UseVisualStyleBackColor = true;
            this.btnTxtDir.Click += new System.EventHandler(this.btnTxtDir_Click);
            // 
            // txtIDIDir
            // 
            this.txtIDIDir.Location = new System.Drawing.Point(356, 21);
            this.txtIDIDir.Name = "txtIDIDir";
            this.txtIDIDir.Size = new System.Drawing.Size(160, 20);
            this.txtIDIDir.TabIndex = 8;
            // 
            // txtTxtDir
            // 
            this.txtTxtDir.Location = new System.Drawing.Point(80, 21);
            this.txtTxtDir.Name = "txtTxtDir";
            this.txtTxtDir.ReadOnly = true;
            this.txtTxtDir.Size = new System.Drawing.Size(160, 20);
            this.txtTxtDir.TabIndex = 7;
            // 
            // lblIDIDir
            // 
            this.lblIDIDir.AutoSize = true;
            this.lblIDIDir.Location = new System.Drawing.Point(284, 24);
            this.lblIDIDir.Name = "lblIDIDir";
            this.lblIDIDir.Size = new System.Drawing.Size(66, 13);
            this.lblIDIDir.TabIndex = 6;
            this.lblIDIDir.Text = "IDI Directory";
            // 
            // lblTxtDir
            // 
            this.lblTxtDir.AutoSize = true;
            this.lblTxtDir.Location = new System.Drawing.Point(11, 24);
            this.lblTxtDir.Name = "lblTxtDir";
            this.lblTxtDir.Size = new System.Drawing.Size(63, 13);
            this.lblTxtDir.TabIndex = 5;
            this.lblTxtDir.Text = "txt Directory";
            // 
            // tFailedRename
            // 
            this.tFailedRename.Location = new System.Drawing.Point(11, 64);
            this.tFailedRename.Multiline = true;
            this.tFailedRename.Name = "tFailedRename";
            this.tFailedRename.ReadOnly = true;
            this.tFailedRename.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tFailedRename.Size = new System.Drawing.Size(639, 317);
            this.tFailedRename.TabIndex = 4;
            // 
            // tpInq
            // 
            this.tpInq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.tpInq.Controls.Add(this.lblAppCount);
            this.tpInq.Controls.Add(this.panel2);
            this.tpInq.Controls.Add(this.btnInqExport);
            this.tpInq.Controls.Add(this.btnInqBack);
            this.tpInq.Controls.Add(this.panel1);
            this.tpInq.Location = new System.Drawing.Point(116, 4);
            this.tpInq.Name = "tpInq";
            this.tpInq.Padding = new System.Windows.Forms.Padding(3);
            this.tpInq.Size = new System.Drawing.Size(670, 412);
            this.tpInq.TabIndex = 3;
            this.tpInq.Text = "Inquiry";
            this.tpInq.UseVisualStyleBackColor = true;
            // 
            // lblAppCount
            // 
            this.lblAppCount.Location = new System.Drawing.Point(485, 379);
            this.lblAppCount.Name = "lblAppCount";
            this.lblAppCount.Size = new System.Drawing.Size(162, 13);
            this.lblAppCount.TabIndex = 13;
            this.lblAppCount.Text = "..";
            this.lblAppCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvInquiry);
            this.panel2.Location = new System.Drawing.Point(4, 141);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(643, 232);
            this.panel2.TabIndex = 4;
            // 
            // dgvInquiry
            // 
            this.dgvInquiry.AllowUserToAddRows = false;
            this.dgvInquiry.AllowUserToDeleteRows = false;
            this.dgvInquiry.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvInquiry.ColumnHeadersHeight = 25;
            this.dgvInquiry.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInquiry.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInquiry.Location = new System.Drawing.Point(0, 0);
            this.dgvInquiry.Name = "dgvInquiry";
            this.dgvInquiry.ReadOnly = true;
            this.dgvInquiry.RowHeadersWidth = 20;
            this.dgvInquiry.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInquiry.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInquiry.ShowEditingIcon = false;
            this.dgvInquiry.Size = new System.Drawing.Size(643, 232);
            this.dgvInquiry.TabIndex = 0;
            // 
            // btnInqExport
            // 
            this.btnInqExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInqExport.Location = new System.Drawing.Point(87, 379);
            this.btnInqExport.Name = "btnInqExport";
            this.btnInqExport.Size = new System.Drawing.Size(75, 23);
            this.btnInqExport.TabIndex = 12;
            this.btnInqExport.Text = "Export";
            this.btnInqExport.UseVisualStyleBackColor = true;
            this.btnInqExport.Click += new System.EventHandler(this.btnInqExport_Click);
            // 
            // btnInqBack
            // 
            this.btnInqBack.Enabled = false;
            this.btnInqBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInqBack.Location = new System.Drawing.Point(6, 379);
            this.btnInqBack.Name = "btnInqBack";
            this.btnInqBack.Size = new System.Drawing.Size(75, 23);
            this.btnInqBack.TabIndex = 11;
            this.btnInqBack.Text = "Move Back";
            this.btnInqBack.UseVisualStyleBackColor = true;
            this.btnInqBack.Click += new System.EventHandler(this.btnInqBack_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtInqName);
            this.panel1.Controls.Add(this.lblInqName);
            this.panel1.Controls.Add(this.gbFilterInq);
            this.panel1.Controls.Add(this.txtInqRegno);
            this.panel1.Controls.Add(this.lblInqRegno);
            this.panel1.Controls.Add(this.txtInqTxtName);
            this.panel1.Controls.Add(this.lblInqTxtName);
            this.panel1.Controls.Add(this.dtpInqTo);
            this.panel1.Controls.Add(this.dtpInqFrom);
            this.panel1.Controls.Add(this.btnInqSearch);
            this.panel1.Controls.Add(this.cmbInqStatus);
            this.panel1.Controls.Add(this.txtInqBatName);
            this.panel1.Controls.Add(this.lblInqTo);
            this.panel1.Controls.Add(this.lblInqDate);
            this.panel1.Controls.Add(this.lblInqBatName);
            this.panel1.Controls.Add(this.lblInqStatus);
            this.panel1.Location = new System.Drawing.Point(4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(643, 132);
            this.panel1.TabIndex = 0;
            // 
            // txtInqName
            // 
            this.txtInqName.Location = new System.Drawing.Point(101, 57);
            this.txtInqName.MaxLength = 50;
            this.txtInqName.Name = "txtInqName";
            this.txtInqName.Size = new System.Drawing.Size(121, 20);
            this.txtInqName.TabIndex = 15;
            // 
            // lblInqName
            // 
            this.lblInqName.AutoSize = true;
            this.lblInqName.Location = new System.Drawing.Point(17, 61);
            this.lblInqName.Name = "lblInqName";
            this.lblInqName.Size = new System.Drawing.Size(35, 13);
            this.lblInqName.TabIndex = 14;
            this.lblInqName.Text = "Name";
            // 
            // gbFilterInq
            // 
            this.gbFilterInq.Controls.Add(this.lblFilterName);
            this.gbFilterInq.Controls.Add(this.lblFilterRegno);
            this.gbFilterInq.Controls.Add(this.txtFilterName);
            this.gbFilterInq.Controls.Add(this.txtFilterRegno);
            this.gbFilterInq.Location = new System.Drawing.Point(357, 3);
            this.gbFilterInq.Name = "gbFilterInq";
            this.gbFilterInq.Size = new System.Drawing.Size(283, 98);
            this.gbFilterInq.TabIndex = 13;
            this.gbFilterInq.TabStop = false;
            this.gbFilterInq.Text = "Filter";
            // 
            // lblFilterName
            // 
            this.lblFilterName.AutoSize = true;
            this.lblFilterName.Location = new System.Drawing.Point(151, 19);
            this.lblFilterName.Name = "lblFilterName";
            this.lblFilterName.Size = new System.Drawing.Size(49, 13);
            this.lblFilterName.TabIndex = 3;
            this.lblFilterName.Text = "by Name";
            // 
            // lblFilterRegno
            // 
            this.lblFilterRegno.AutoSize = true;
            this.lblFilterRegno.Location = new System.Drawing.Point(16, 20);
            this.lblFilterRegno.Name = "lblFilterRegno";
            this.lblFilterRegno.Size = new System.Drawing.Size(53, 13);
            this.lblFilterRegno.TabIndex = 2;
            this.lblFilterRegno.Text = "by Regno";
            // 
            // txtFilterName
            // 
            this.txtFilterName.Location = new System.Drawing.Point(151, 46);
            this.txtFilterName.MaxLength = 50;
            this.txtFilterName.Name = "txtFilterName";
            this.txtFilterName.Size = new System.Drawing.Size(120, 20);
            this.txtFilterName.TabIndex = 1;
            // 
            // txtFilterRegno
            // 
            this.txtFilterRegno.Location = new System.Drawing.Point(16, 46);
            this.txtFilterRegno.MaxLength = 50;
            this.txtFilterRegno.Name = "txtFilterRegno";
            this.txtFilterRegno.Size = new System.Drawing.Size(120, 20);
            this.txtFilterRegno.TabIndex = 0;
            // 
            // txtInqRegno
            // 
            this.txtInqRegno.Location = new System.Drawing.Point(101, 33);
            this.txtInqRegno.MaxLength = 25;
            this.txtInqRegno.Name = "txtInqRegno";
            this.txtInqRegno.Size = new System.Drawing.Size(121, 20);
            this.txtInqRegno.TabIndex = 12;
            // 
            // lblInqRegno
            // 
            this.lblInqRegno.AutoSize = true;
            this.lblInqRegno.Location = new System.Drawing.Point(17, 36);
            this.lblInqRegno.Name = "lblInqRegno";
            this.lblInqRegno.Size = new System.Drawing.Size(39, 13);
            this.lblInqRegno.TabIndex = 11;
            this.lblInqRegno.Text = "Regno";
            // 
            // txtInqTxtName
            // 
            this.txtInqTxtName.Location = new System.Drawing.Point(322, 53);
            this.txtInqTxtName.Name = "txtInqTxtName";
            this.txtInqTxtName.Size = new System.Drawing.Size(121, 20);
            this.txtInqTxtName.TabIndex = 7;
            this.txtInqTxtName.Visible = false;
            // 
            // lblInqTxtName
            // 
            this.lblInqTxtName.AutoSize = true;
            this.lblInqTxtName.Location = new System.Drawing.Point(238, 56);
            this.lblInqTxtName.Name = "lblInqTxtName";
            this.lblInqTxtName.Size = new System.Drawing.Size(59, 13);
            this.lblInqTxtName.TabIndex = 2;
            this.lblInqTxtName.Text = "Text Name";
            this.lblInqTxtName.Visible = false;
            // 
            // dtpInqTo
            // 
            this.dtpInqTo.Checked = false;
            this.dtpInqTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInqTo.Location = new System.Drawing.Point(228, 81);
            this.dtpInqTo.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtpInqTo.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpInqTo.Name = "dtpInqTo";
            this.dtpInqTo.Size = new System.Drawing.Size(100, 20);
            this.dtpInqTo.TabIndex = 9;
            // 
            // dtpInqFrom
            // 
            this.dtpInqFrom.Checked = false;
            this.dtpInqFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInqFrom.Location = new System.Drawing.Point(101, 81);
            this.dtpInqFrom.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtpInqFrom.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpInqFrom.Name = "dtpInqFrom";
            this.dtpInqFrom.Size = new System.Drawing.Size(100, 20);
            this.dtpInqFrom.TabIndex = 8;
            // 
            // btnInqSearch
            // 
            this.btnInqSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInqSearch.Location = new System.Drawing.Point(20, 106);
            this.btnInqSearch.Name = "btnInqSearch";
            this.btnInqSearch.Size = new System.Drawing.Size(75, 23);
            this.btnInqSearch.TabIndex = 10;
            this.btnInqSearch.Text = "Search";
            this.btnInqSearch.UseVisualStyleBackColor = true;
            this.btnInqSearch.Click += new System.EventHandler(this.btnInqSearch_Click);
            // 
            // cmbInqStatus
            // 
            this.cmbInqStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbInqStatus.FormattingEnabled = true;
            this.cmbInqStatus.Items.AddRange(new object[] {
            "Waiting Process",
            "In Progress",
            "Finished"});
            this.cmbInqStatus.Location = new System.Drawing.Point(101, 8);
            this.cmbInqStatus.Name = "cmbInqStatus";
            this.cmbInqStatus.Size = new System.Drawing.Size(121, 21);
            this.cmbInqStatus.TabIndex = 5;
            // 
            // txtInqBatName
            // 
            this.txtInqBatName.Location = new System.Drawing.Point(322, 30);
            this.txtInqBatName.MaxLength = 100;
            this.txtInqBatName.Name = "txtInqBatName";
            this.txtInqBatName.Size = new System.Drawing.Size(121, 20);
            this.txtInqBatName.TabIndex = 6;
            this.txtInqBatName.Visible = false;
            // 
            // lblInqTo
            // 
            this.lblInqTo.AutoSize = true;
            this.lblInqTo.Location = new System.Drawing.Point(206, 86);
            this.lblInqTo.Name = "lblInqTo";
            this.lblInqTo.Size = new System.Drawing.Size(16, 13);
            this.lblInqTo.TabIndex = 4;
            this.lblInqTo.Text = "to";
            // 
            // lblInqDate
            // 
            this.lblInqDate.AutoSize = true;
            this.lblInqDate.Location = new System.Drawing.Point(17, 86);
            this.lblInqDate.Name = "lblInqDate";
            this.lblInqDate.Size = new System.Drawing.Size(60, 13);
            this.lblInqDate.TabIndex = 3;
            this.lblInqDate.Text = "Finish Date";
            // 
            // lblInqBatName
            // 
            this.lblInqBatName.AutoSize = true;
            this.lblInqBatName.Location = new System.Drawing.Point(238, 33);
            this.lblInqBatName.Name = "lblInqBatName";
            this.lblInqBatName.Size = new System.Drawing.Size(49, 13);
            this.lblInqBatName.TabIndex = 1;
            this.lblInqBatName.Text = "Batch ID";
            this.lblInqBatName.Visible = false;
            // 
            // lblInqStatus
            // 
            this.lblInqStatus.AutoSize = true;
            this.lblInqStatus.Location = new System.Drawing.Point(17, 11);
            this.lblInqStatus.Name = "lblInqStatus";
            this.lblInqStatus.Size = new System.Drawing.Size(37, 13);
            this.lblInqStatus.TabIndex = 0;
            this.lblInqStatus.Text = "Status";
            // 
            // tpSetting
            // 
            this.tpSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.tpSetting.Controls.Add(this.gbWebIDB);
            this.tpSetting.Controls.Add(this.gbWebBKM);
            this.tpSetting.Controls.Add(this.gbMisc);
            this.tpSetting.Controls.Add(this.gbTime);
            this.tpSetting.Controls.Add(this.gbUserWeb);
            this.tpSetting.Controls.Add(this.gbSearchCrit);
            this.tpSetting.Controls.Add(this.gbFolder);
            this.tpSetting.Controls.Add(this.btnEdit);
            this.tpSetting.Controls.Add(this.chkDwn);
            this.tpSetting.Controls.Add(this.chkAppr);
            this.tpSetting.Controls.Add(this.chkReq);
            this.tpSetting.Controls.Add(this.gbUserApr);
            this.tpSetting.Controls.Add(this.btnCancel);
            this.tpSetting.Controls.Add(this.btnSave);
            this.tpSetting.Controls.Add(this.gbConn);
            this.tpSetting.Controls.Add(this.gbUserReq);
            this.tpSetting.Location = new System.Drawing.Point(116, 4);
            this.tpSetting.Name = "tpSetting";
            this.tpSetting.Padding = new System.Windows.Forms.Padding(3);
            this.tpSetting.Size = new System.Drawing.Size(670, 423);
            this.tpSetting.TabIndex = 1;
            this.tpSetting.Text = "Setting";
            this.tpSetting.UseVisualStyleBackColor = true;
            // 
            // gbWebIDB
            // 
            this.gbWebIDB.Controls.Add(this.nMaxPageIDB);
            this.gbWebIDB.Controls.Add(this.lblMaxLoopIDB);
            this.gbWebIDB.Controls.Add(this.nMaxLoopIDB);
            this.gbWebIDB.Controls.Add(this.lblMaxPageIDB);
            this.gbWebIDB.Location = new System.Drawing.Point(335, 330);
            this.gbWebIDB.Name = "gbWebIDB";
            this.gbWebIDB.Size = new System.Drawing.Size(330, 39);
            this.gbWebIDB.TabIndex = 15;
            this.gbWebIDB.TabStop = false;
            this.gbWebIDB.Text = "IDB - Settings";
            // 
            // nMaxPageIDB
            // 
            this.nMaxPageIDB.Location = new System.Drawing.Point(251, 13);
            this.nMaxPageIDB.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nMaxPageIDB.Name = "nMaxPageIDB";
            this.nMaxPageIDB.Size = new System.Drawing.Size(47, 20);
            this.nMaxPageIDB.TabIndex = 20;
            this.nMaxPageIDB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblMaxLoopIDB
            // 
            this.lblMaxLoopIDB.AutoSize = true;
            this.lblMaxLoopIDB.Location = new System.Drawing.Point(15, 16);
            this.lblMaxLoopIDB.Name = "lblMaxLoopIDB";
            this.lblMaxLoopIDB.Size = new System.Drawing.Size(54, 13);
            this.lblMaxLoopIDB.TabIndex = 17;
            this.lblMaxLoopIDB.Text = "Max Loop";
            // 
            // nMaxLoopIDB
            // 
            this.nMaxLoopIDB.Location = new System.Drawing.Point(85, 13);
            this.nMaxLoopIDB.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nMaxLoopIDB.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nMaxLoopIDB.Name = "nMaxLoopIDB";
            this.nMaxLoopIDB.Size = new System.Drawing.Size(47, 20);
            this.nMaxLoopIDB.TabIndex = 19;
            this.nMaxLoopIDB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblMaxPageIDB
            // 
            this.lblMaxPageIDB.AutoSize = true;
            this.lblMaxPageIDB.Location = new System.Drawing.Point(171, 16);
            this.lblMaxPageIDB.Name = "lblMaxPageIDB";
            this.lblMaxPageIDB.Size = new System.Drawing.Size(60, 13);
            this.lblMaxPageIDB.TabIndex = 18;
            this.lblMaxPageIDB.Text = "Max Pages";
            // 
            // gbWebBKM
            // 
            this.gbWebBKM.Controls.Add(this.nMaxPageBKM);
            this.gbWebBKM.Controls.Add(this.lblMaxLoopBKM);
            this.gbWebBKM.Controls.Add(this.nMaxLoopBKM);
            this.gbWebBKM.Controls.Add(this.lblMaxPageBKM);
            this.gbWebBKM.Location = new System.Drawing.Point(2, 342);
            this.gbWebBKM.Name = "gbWebBKM";
            this.gbWebBKM.Size = new System.Drawing.Size(329, 39);
            this.gbWebBKM.TabIndex = 14;
            this.gbWebBKM.TabStop = false;
            this.gbWebBKM.Text = "BKM - Settings";
            // 
            // nMaxPageBKM
            // 
            this.nMaxPageBKM.Location = new System.Drawing.Point(238, 13);
            this.nMaxPageBKM.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nMaxPageBKM.Name = "nMaxPageBKM";
            this.nMaxPageBKM.Size = new System.Drawing.Size(47, 20);
            this.nMaxPageBKM.TabIndex = 16;
            this.nMaxPageBKM.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblMaxLoopBKM
            // 
            this.lblMaxLoopBKM.AutoSize = true;
            this.lblMaxLoopBKM.Location = new System.Drawing.Point(17, 16);
            this.lblMaxLoopBKM.Name = "lblMaxLoopBKM";
            this.lblMaxLoopBKM.Size = new System.Drawing.Size(54, 13);
            this.lblMaxLoopBKM.TabIndex = 13;
            this.lblMaxLoopBKM.Text = "Max Loop";
            // 
            // nMaxLoopBKM
            // 
            this.nMaxLoopBKM.Location = new System.Drawing.Point(92, 13);
            this.nMaxLoopBKM.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nMaxLoopBKM.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nMaxLoopBKM.Name = "nMaxLoopBKM";
            this.nMaxLoopBKM.Size = new System.Drawing.Size(47, 20);
            this.nMaxLoopBKM.TabIndex = 15;
            this.nMaxLoopBKM.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblMaxPageBKM
            // 
            this.lblMaxPageBKM.AutoSize = true;
            this.lblMaxPageBKM.Location = new System.Drawing.Point(161, 15);
            this.lblMaxPageBKM.Name = "lblMaxPageBKM";
            this.lblMaxPageBKM.Size = new System.Drawing.Size(60, 13);
            this.lblMaxPageBKM.TabIndex = 14;
            this.lblMaxPageBKM.Text = "Max Pages";
            // 
            // gbMisc
            // 
            this.gbMisc.Controls.Add(this.nMinDataGen);
            this.gbMisc.Controls.Add(this.cbAutoConvert);
            this.gbMisc.Controls.Add(this.nLoop);
            this.gbMisc.Controls.Add(this.lblLoop);
            this.gbMisc.Controls.Add(this.nDelay);
            this.gbMisc.Controls.Add(this.lblDelayTime);
            this.gbMisc.Controls.Add(this.nMaxTxtGen);
            this.gbMisc.Controls.Add(this.lblDtGen);
            this.gbMisc.Controls.Add(this.lblTxtGen);
            this.gbMisc.Controls.Add(this.nMaxDataGen);
            this.gbMisc.Controls.Add(this.cbResume);
            this.gbMisc.Location = new System.Drawing.Point(335, 259);
            this.gbMisc.Name = "gbMisc";
            this.gbMisc.Size = new System.Drawing.Size(330, 70);
            this.gbMisc.TabIndex = 8;
            this.gbMisc.TabStop = false;
            this.gbMisc.Text = "Misc";
            // 
            // nMinDataGen
            // 
            this.nMinDataGen.Location = new System.Drawing.Point(70, 17);
            this.nMinDataGen.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nMinDataGen.Name = "nMinDataGen";
            this.nMinDataGen.Size = new System.Drawing.Size(40, 20);
            this.nMinDataGen.TabIndex = 17;
            this.nMinDataGen.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbAutoConvert
            // 
            this.cbAutoConvert.Location = new System.Drawing.Point(264, 36);
            this.cbAutoConvert.Name = "cbAutoConvert";
            this.cbAutoConvert.Size = new System.Drawing.Size(65, 30);
            this.cbAutoConvert.TabIndex = 16;
            this.cbAutoConvert.Text = "Auto Convert";
            this.cbAutoConvert.UseVisualStyleBackColor = true;
            // 
            // nLoop
            // 
            this.nLoop.Location = new System.Drawing.Point(205, 43);
            this.nLoop.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nLoop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nLoop.Name = "nLoop";
            this.nLoop.Size = new System.Drawing.Size(50, 20);
            this.nLoop.TabIndex = 5;
            this.nLoop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nLoop.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblLoop
            // 
            this.lblLoop.AutoSize = true;
            this.lblLoop.Location = new System.Drawing.Point(161, 46);
            this.lblLoop.Name = "lblLoop";
            this.lblLoop.Size = new System.Drawing.Size(31, 13);
            this.lblLoop.TabIndex = 12;
            this.lblLoop.Text = "Loop";
            // 
            // nDelay
            // 
            this.nDelay.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nDelay.Location = new System.Drawing.Point(70, 43);
            this.nDelay.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nDelay.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nDelay.Name = "nDelay";
            this.nDelay.Size = new System.Drawing.Size(60, 20);
            this.nDelay.TabIndex = 4;
            this.nDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nDelay.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // lblDelayTime
            // 
            this.lblDelayTime.AutoSize = true;
            this.lblDelayTime.Location = new System.Drawing.Point(4, 46);
            this.lblDelayTime.Name = "lblDelayTime";
            this.lblDelayTime.Size = new System.Drawing.Size(56, 13);
            this.lblDelayTime.TabIndex = 9;
            this.lblDelayTime.Text = "Delay time";
            // 
            // nMaxTxtGen
            // 
            this.nMaxTxtGen.Location = new System.Drawing.Point(205, 17);
            this.nMaxTxtGen.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nMaxTxtGen.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nMaxTxtGen.Name = "nMaxTxtGen";
            this.nMaxTxtGen.Size = new System.Drawing.Size(50, 20);
            this.nMaxTxtGen.TabIndex = 2;
            this.nMaxTxtGen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nMaxTxtGen.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblDtGen
            // 
            this.lblDtGen.AutoSize = true;
            this.lblDtGen.Location = new System.Drawing.Point(4, 19);
            this.lblDtGen.Name = "lblDtGen";
            this.lblDtGen.Size = new System.Drawing.Size(63, 13);
            this.lblDtGen.TabIndex = 5;
            this.lblDtGen.Text = "Line per Txt";
            // 
            // lblTxtGen
            // 
            this.lblTxtGen.AutoSize = true;
            this.lblTxtGen.Location = new System.Drawing.Point(161, 19);
            this.lblTxtGen.Name = "lblTxtGen";
            this.lblTxtGen.Size = new System.Drawing.Size(45, 13);
            this.lblTxtGen.TabIndex = 8;
            this.lblTxtGen.Text = "Txt Gen";
            // 
            // nMaxDataGen
            // 
            this.nMaxDataGen.Location = new System.Drawing.Point(113, 17);
            this.nMaxDataGen.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nMaxDataGen.Name = "nMaxDataGen";
            this.nMaxDataGen.Size = new System.Drawing.Size(50, 20);
            this.nMaxDataGen.TabIndex = 1;
            this.nMaxDataGen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nMaxDataGen.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // cbResume
            // 
            this.cbResume.Location = new System.Drawing.Point(264, 7);
            this.cbResume.Name = "cbResume";
            this.cbResume.Size = new System.Drawing.Size(65, 30);
            this.cbResume.TabIndex = 3;
            this.cbResume.Text = "Auto Resume";
            this.cbResume.UseVisualStyleBackColor = true;
            // 
            // gbTime
            // 
            this.gbTime.Controls.Add(this.lblFreqEvery);
            this.gbTime.Controls.Add(this.lblFreqMinute);
            this.gbTime.Controls.Add(this.nFreqMinute);
            this.gbTime.Controls.Add(this.lblsd);
            this.gbTime.Controls.Add(this.lblStart);
            this.gbTime.Controls.Add(this.dtpEnd);
            this.gbTime.Controls.Add(this.lblDays);
            this.gbTime.Controls.Add(this.ccbDays);
            this.gbTime.Controls.Add(this.dtpStart);
            this.gbTime.Location = new System.Drawing.Point(335, 187);
            this.gbTime.Name = "gbTime";
            this.gbTime.Size = new System.Drawing.Size(330, 71);
            this.gbTime.TabIndex = 7;
            this.gbTime.TabStop = false;
            this.gbTime.Text = "Time Setting";
            // 
            // lblFreqEvery
            // 
            this.lblFreqEvery.AutoSize = true;
            this.lblFreqEvery.Location = new System.Drawing.Point(184, 46);
            this.lblFreqEvery.Name = "lblFreqEvery";
            this.lblFreqEvery.Size = new System.Drawing.Size(33, 13);
            this.lblFreqEvery.TabIndex = 9;
            this.lblFreqEvery.Text = "every";
            // 
            // lblFreqMinute
            // 
            this.lblFreqMinute.AutoSize = true;
            this.lblFreqMinute.Location = new System.Drawing.Point(263, 46);
            this.lblFreqMinute.Name = "lblFreqMinute";
            this.lblFreqMinute.Size = new System.Drawing.Size(50, 13);
            this.lblFreqMinute.TabIndex = 8;
            this.lblFreqMinute.Text = "Minute(s)";
            // 
            // nFreqMinute
            // 
            this.nFreqMinute.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nFreqMinute.Location = new System.Drawing.Point(218, 43);
            this.nFreqMinute.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.nFreqMinute.Name = "nFreqMinute";
            this.nFreqMinute.Size = new System.Drawing.Size(47, 20);
            this.nFreqMinute.TabIndex = 7;
            // 
            // lblsd
            // 
            this.lblsd.AutoSize = true;
            this.lblsd.Location = new System.Drawing.Point(98, 46);
            this.lblsd.Name = "lblsd";
            this.lblsd.Size = new System.Drawing.Size(16, 13);
            this.lblsd.TabIndex = 5;
            this.lblsd.Text = "to";
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(5, 47);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(30, 13);
            this.lblStart.TabIndex = 4;
            this.lblStart.Text = "From";
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "HH : mm";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(116, 43);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.ShowUpDown = true;
            this.dtpEnd.Size = new System.Drawing.Size(60, 20);
            this.dtpEnd.TabIndex = 6;
            this.dtpEnd.Value = new System.DateTime(2014, 1, 16, 17, 0, 0, 0);
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Location = new System.Drawing.Point(5, 20);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(31, 13);
            this.lblDays.TabIndex = 3;
            this.lblDays.Text = "Days";
            // 
            // ccbDays
            // 
            this.ccbDays.CheckOnClick = true;
            this.ccbDays.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.ccbDays.DropDownHeight = 1;
            this.ccbDays.FormattingEnabled = true;
            this.ccbDays.IntegralHeight = false;
            this.ccbDays.Location = new System.Drawing.Point(36, 17);
            this.ccbDays.Name = "ccbDays";
            this.ccbDays.Size = new System.Drawing.Size(262, 21);
            this.ccbDays.TabIndex = 2;
            this.ccbDays.ValueSeparator = ", ";
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "HH : mm";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(36, 43);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.ShowUpDown = true;
            this.dtpStart.Size = new System.Drawing.Size(60, 20);
            this.dtpStart.TabIndex = 5;
            this.dtpStart.Value = new System.DateTime(2014, 1, 16, 7, 0, 0, 0);
            // 
            // gbUserWeb
            // 
            this.gbUserWeb.Controls.Add(this.txtKdCbgWeb);
            this.gbUserWeb.Controls.Add(this.txtKdBankWeb);
            this.gbUserWeb.Controls.Add(this.lblKodeCabangWeb);
            this.gbUserWeb.Controls.Add(this.lblKodeBankWeb);
            this.gbUserWeb.Controls.Add(this.cbEqBatch);
            this.gbUserWeb.Controls.Add(this.txtPwdWeb);
            this.gbUserWeb.Controls.Add(this.txtUserWeb);
            this.gbUserWeb.Controls.Add(this.lblPwdWeb);
            this.gbUserWeb.Controls.Add(this.lblUserWeb);
            this.gbUserWeb.Location = new System.Drawing.Point(2, 78);
            this.gbUserWeb.Name = "gbUserWeb";
            this.gbUserWeb.Size = new System.Drawing.Size(329, 71);
            this.gbUserWeb.TabIndex = 2;
            this.gbUserWeb.TabStop = false;
            this.gbUserWeb.Text = "User Web BI";
            // 
            // txtKdCbgWeb
            // 
            this.txtKdCbgWeb.Location = new System.Drawing.Point(92, 43);
            this.txtKdCbgWeb.MaxLength = 5;
            this.txtKdCbgWeb.Name = "txtKdCbgWeb";
            this.txtKdCbgWeb.Size = new System.Drawing.Size(60, 20);
            this.txtKdCbgWeb.TabIndex = 5;
            // 
            // txtKdBankWeb
            // 
            this.txtKdBankWeb.Location = new System.Drawing.Point(92, 18);
            this.txtKdBankWeb.MaxLength = 5;
            this.txtKdBankWeb.Name = "txtKdBankWeb";
            this.txtKdBankWeb.Size = new System.Drawing.Size(60, 20);
            this.txtKdBankWeb.TabIndex = 4;
            // 
            // lblKodeCabangWeb
            // 
            this.lblKodeCabangWeb.AutoSize = true;
            this.lblKodeCabangWeb.Location = new System.Drawing.Point(17, 46);
            this.lblKodeCabangWeb.Name = "lblKodeCabangWeb";
            this.lblKodeCabangWeb.Size = new System.Drawing.Size(72, 13);
            this.lblKodeCabangWeb.TabIndex = 19;
            this.lblKodeCabangWeb.Text = "Kode Cabang";
            // 
            // lblKodeBankWeb
            // 
            this.lblKodeBankWeb.AutoSize = true;
            this.lblKodeBankWeb.Location = new System.Drawing.Point(17, 21);
            this.lblKodeBankWeb.Name = "lblKodeBankWeb";
            this.lblKodeBankWeb.Size = new System.Drawing.Size(60, 13);
            this.lblKodeBankWeb.TabIndex = 18;
            this.lblKodeBankWeb.Text = "Kode Bank";
            // 
            // cbEqBatch
            // 
            this.cbEqBatch.AutoSize = true;
            this.cbEqBatch.Location = new System.Drawing.Point(87, -1);
            this.cbEqBatch.Name = "cbEqBatch";
            this.cbEqBatch.Size = new System.Drawing.Size(125, 17);
            this.cbEqBatch.TabIndex = 1;
            this.cbEqBatch.Text = "Sama dengan Maker";
            this.cbEqBatch.UseVisualStyleBackColor = true;
            this.cbEqBatch.CheckedChanged += new System.EventHandler(this.cbEqBatch_CheckedChanged);
            // 
            // txtPwdWeb
            // 
            this.txtPwdWeb.Enabled = false;
            this.txtPwdWeb.Location = new System.Drawing.Point(221, 43);
            this.txtPwdWeb.MaxLength = 20;
            this.txtPwdWeb.Name = "txtPwdWeb";
            this.txtPwdWeb.PasswordChar = '*';
            this.txtPwdWeb.Size = new System.Drawing.Size(100, 20);
            this.txtPwdWeb.TabIndex = 7;
            // 
            // txtUserWeb
            // 
            this.txtUserWeb.Enabled = false;
            this.txtUserWeb.Location = new System.Drawing.Point(221, 18);
            this.txtUserWeb.MaxLength = 20;
            this.txtUserWeb.Name = "txtUserWeb";
            this.txtUserWeb.Size = new System.Drawing.Size(100, 20);
            this.txtUserWeb.TabIndex = 6;
            // 
            // lblPwdWeb
            // 
            this.lblPwdWeb.AutoSize = true;
            this.lblPwdWeb.Location = new System.Drawing.Point(161, 46);
            this.lblPwdWeb.Name = "lblPwdWeb";
            this.lblPwdWeb.Size = new System.Drawing.Size(54, 13);
            this.lblPwdWeb.TabIndex = 14;
            this.lblPwdWeb.Text = "Pwd Web";
            // 
            // lblUserWeb
            // 
            this.lblUserWeb.AutoSize = true;
            this.lblUserWeb.Location = new System.Drawing.Point(161, 21);
            this.lblUserWeb.Name = "lblUserWeb";
            this.lblUserWeb.Size = new System.Drawing.Size(55, 13);
            this.lblUserWeb.TabIndex = 13;
            this.lblUserWeb.Text = "User Web";
            // 
            // gbSearchCrit
            // 
            this.gbSearchCrit.Controls.Add(this.cbCO);
            this.gbSearchCrit.Controls.Add(this.cbMF);
            this.gbSearchCrit.Controls.Add(this.txtInitialLetter);
            this.gbSearchCrit.Controls.Add(this.lblInitialLetter);
            this.gbSearchCrit.Controls.Add(this.txtUIDCBS);
            this.gbSearchCrit.Controls.Add(this.cbUseCuType);
            this.gbSearchCrit.Controls.Add(this.cbLOSInt);
            this.gbSearchCrit.Controls.Add(this.lblModule);
            this.gbSearchCrit.Controls.Add(this.cbPL);
            this.gbSearchCrit.Controls.Add(this.cbCritNPWP);
            this.gbSearchCrit.Controls.Add(this.cbCC);
            this.gbSearchCrit.Controls.Add(this.cbCritKTP);
            this.gbSearchCrit.Controls.Add(this.cbCritGender);
            this.gbSearchCrit.Controls.Add(this.cbCritDOB);
            this.gbSearchCrit.Controls.Add(this.cbCritName);
            this.gbSearchCrit.Location = new System.Drawing.Point(3, 221);
            this.gbSearchCrit.Name = "gbSearchCrit";
            this.gbSearchCrit.Size = new System.Drawing.Size(329, 120);
            this.gbSearchCrit.TabIndex = 4;
            this.gbSearchCrit.TabStop = false;
            this.gbSearchCrit.Text = "Search Criteria";
            // 
            // txtInitialLetter
            // 
            this.txtInitialLetter.Location = new System.Drawing.Point(69, 83);
            this.txtInitialLetter.MaxLength = 100;
            this.txtInitialLetter.Name = "txtInitialLetter";
            this.txtInitialLetter.Size = new System.Drawing.Size(100, 20);
            this.txtInitialLetter.TabIndex = 14;
            // 
            // lblInitialLetter
            // 
            this.lblInitialLetter.AutoSize = true;
            this.lblInitialLetter.Location = new System.Drawing.Point(6, 87);
            this.lblInitialLetter.Name = "lblInitialLetter";
            this.lblInitialLetter.Size = new System.Drawing.Size(59, 13);
            this.lblInitialLetter.TabIndex = 13;
            this.lblInitialLetter.Text = "Initial Surat";
            // 
            // txtUIDCBS
            // 
            this.txtUIDCBS.Location = new System.Drawing.Point(148, 58);
            this.txtUIDCBS.MaxLength = 20;
            this.txtUIDCBS.Name = "txtUIDCBS";
            this.txtUIDCBS.Size = new System.Drawing.Size(136, 20);
            this.txtUIDCBS.TabIndex = 8;
            // 
            // cbUseCuType
            // 
            this.cbUseCuType.Location = new System.Drawing.Point(240, 6);
            this.cbUseCuType.Name = "cbUseCuType";
            this.cbUseCuType.Size = new System.Drawing.Size(83, 39);
            this.cbUseCuType.TabIndex = 12;
            this.cbUseCuType.Text = "Use Cust Type";
            this.cbUseCuType.UseVisualStyleBackColor = true;
            // 
            // cbLOSInt
            // 
            this.cbLOSInt.AutoSize = true;
            this.cbLOSInt.Location = new System.Drawing.Point(9, 60);
            this.cbLOSInt.Name = "cbLOSInt";
            this.cbLOSInt.Size = new System.Drawing.Size(125, 17);
            this.cbLOSInt.TabIndex = 9;
            this.cbLOSInt.Text = "User LOS Integration";
            this.cbLOSInt.UseVisualStyleBackColor = true;
            this.cbLOSInt.CheckedChanged += new System.EventHandler(this.cbLOSInt_CheckedChanged);
            // 
            // lblModule
            // 
            this.lblModule.AutoSize = true;
            this.lblModule.Location = new System.Drawing.Point(178, 83);
            this.lblModule.Name = "lblModule";
            this.lblModule.Size = new System.Drawing.Size(51, 13);
            this.lblModule.TabIndex = 5;
            this.lblModule.Text = "Module : ";
            // 
            // cbPL
            // 
            this.cbPL.AutoSize = true;
            this.cbPL.Location = new System.Drawing.Point(235, 82);
            this.cbPL.Name = "cbPL";
            this.cbPL.Size = new System.Drawing.Size(39, 17);
            this.cbPL.TabIndex = 6;
            this.cbPL.Text = "PL";
            this.cbPL.UseVisualStyleBackColor = true;
            // 
            // cbCritNPWP
            // 
            this.cbCritNPWP.AutoSize = true;
            this.cbCritNPWP.Location = new System.Drawing.Point(75, 37);
            this.cbCritNPWP.Name = "cbCritNPWP";
            this.cbCritNPWP.Size = new System.Drawing.Size(59, 17);
            this.cbCritNPWP.TabIndex = 4;
            this.cbCritNPWP.Text = "NPWP";
            this.cbCritNPWP.UseVisualStyleBackColor = true;
            // 
            // cbCC
            // 
            this.cbCC.AutoSize = true;
            this.cbCC.Location = new System.Drawing.Point(280, 83);
            this.cbCC.Name = "cbCC";
            this.cbCC.Size = new System.Drawing.Size(40, 17);
            this.cbCC.TabIndex = 5;
            this.cbCC.Text = "CC";
            this.cbCC.UseVisualStyleBackColor = true;
            // 
            // cbCritKTP
            // 
            this.cbCritKTP.AutoSize = true;
            this.cbCritKTP.Location = new System.Drawing.Point(9, 37);
            this.cbCritKTP.Name = "cbCritKTP";
            this.cbCritKTP.Size = new System.Drawing.Size(64, 17);
            this.cbCritKTP.TabIndex = 3;
            this.cbCritKTP.Text = "No KTP";
            this.cbCritKTP.UseVisualStyleBackColor = true;
            // 
            // cbCritGender
            // 
            this.cbCritGender.AutoSize = true;
            this.cbCritGender.Location = new System.Drawing.Point(148, 17);
            this.cbCritGender.Name = "cbCritGender";
            this.cbCritGender.Size = new System.Drawing.Size(90, 17);
            this.cbCritGender.TabIndex = 2;
            this.cbCritGender.Text = "Jenis Kelamin";
            this.cbCritGender.UseVisualStyleBackColor = true;
            // 
            // cbCritDOB
            // 
            this.cbCritDOB.AutoSize = true;
            this.cbCritDOB.Location = new System.Drawing.Point(75, 17);
            this.cbCritDOB.Name = "cbCritDOB";
            this.cbCritDOB.Size = new System.Drawing.Size(67, 17);
            this.cbCritDOB.TabIndex = 1;
            this.cbCritDOB.Text = "Tgl Lahir";
            this.cbCritDOB.UseVisualStyleBackColor = true;
            // 
            // cbCritName
            // 
            this.cbCritName.AutoSize = true;
            this.cbCritName.Location = new System.Drawing.Point(9, 17);
            this.cbCritName.Name = "cbCritName";
            this.cbCritName.Size = new System.Drawing.Size(54, 17);
            this.cbCritName.TabIndex = 0;
            this.cbCritName.Text = "Nama";
            this.cbCritName.UseVisualStyleBackColor = true;
            // 
            // gbFolder
            // 
            this.gbFolder.Controls.Add(this.btnBKM);
            this.gbFolder.Controls.Add(this.txtBKMFolder);
            this.gbFolder.Controls.Add(this.btnOutput);
            this.gbFolder.Controls.Add(this.btnInput);
            this.gbFolder.Controls.Add(this.txtOutputFolder);
            this.gbFolder.Controls.Add(this.txtInputFolder);
            this.gbFolder.Location = new System.Drawing.Point(335, 78);
            this.gbFolder.Name = "gbFolder";
            this.gbFolder.Size = new System.Drawing.Size(330, 108);
            this.gbFolder.TabIndex = 6;
            this.gbFolder.TabStop = false;
            this.gbFolder.Text = "Folder Setting";
            // 
            // btnBKM
            // 
            this.btnBKM.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBKM.Location = new System.Drawing.Point(240, 45);
            this.btnBKM.Name = "btnBKM";
            this.btnBKM.Size = new System.Drawing.Size(64, 23);
            this.btnBKM.TabIndex = 4;
            this.btnBKM.Text = "BKM File";
            this.btnBKM.UseVisualStyleBackColor = true;
            this.btnBKM.Click += new System.EventHandler(this.btnBKM_Click);
            // 
            // txtBKMFolder
            // 
            this.txtBKMFolder.Location = new System.Drawing.Point(18, 47);
            this.txtBKMFolder.Name = "txtBKMFolder";
            this.txtBKMFolder.ReadOnly = true;
            this.txtBKMFolder.Size = new System.Drawing.Size(215, 20);
            this.txtBKMFolder.TabIndex = 1;
            this.txtBKMFolder.DoubleClick += new System.EventHandler(this.btnBKM_Click);
            // 
            // btnOutput
            // 
            this.btnOutput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOutput.Location = new System.Drawing.Point(240, 72);
            this.btnOutput.Name = "btnOutput";
            this.btnOutput.Size = new System.Drawing.Size(64, 23);
            this.btnOutput.TabIndex = 5;
            this.btnOutput.Text = "Output SID";
            this.btnOutput.UseVisualStyleBackColor = true;
            this.btnOutput.Click += new System.EventHandler(this.btnOutput_Click);
            // 
            // btnInput
            // 
            this.btnInput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInput.Location = new System.Drawing.Point(240, 18);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(64, 23);
            this.btnInput.TabIndex = 3;
            this.btnInput.Text = "Input Txt";
            this.btnInput.UseVisualStyleBackColor = true;
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Location = new System.Drawing.Point(18, 74);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.ReadOnly = true;
            this.txtOutputFolder.Size = new System.Drawing.Size(215, 20);
            this.txtOutputFolder.TabIndex = 2;
            this.txtOutputFolder.DoubleClick += new System.EventHandler(this.btnOutput_Click);
            // 
            // txtInputFolder
            // 
            this.txtInputFolder.Location = new System.Drawing.Point(18, 20);
            this.txtInputFolder.Name = "txtInputFolder";
            this.txtInputFolder.ReadOnly = true;
            this.txtInputFolder.Size = new System.Drawing.Size(215, 20);
            this.txtInputFolder.TabIndex = 0;
            this.txtInputFolder.DoubleClick += new System.EventHandler(this.btnInput_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.Location = new System.Drawing.Point(201, 391);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(66, 23);
            this.btnEdit.TabIndex = 9;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // chkDwn
            // 
            this.chkDwn.AutoSize = true;
            this.chkDwn.Enabled = false;
            this.chkDwn.Location = new System.Drawing.Point(115, 400);
            this.chkDwn.Name = "chkDwn";
            this.chkDwn.Size = new System.Drawing.Size(74, 17);
            this.chkDwn.TabIndex = 13;
            this.chkDwn.Text = "Download";
            this.chkDwn.UseVisualStyleBackColor = true;
            this.chkDwn.Visible = false;
            // 
            // chkAppr
            // 
            this.chkAppr.AutoSize = true;
            this.chkAppr.Enabled = false;
            this.chkAppr.Location = new System.Drawing.Point(115, 381);
            this.chkAppr.Name = "chkAppr";
            this.chkAppr.Size = new System.Drawing.Size(68, 17);
            this.chkAppr.TabIndex = 12;
            this.chkAppr.Text = "Approval";
            this.chkAppr.UseVisualStyleBackColor = true;
            this.chkAppr.Visible = false;
            // 
            // chkReq
            // 
            this.chkReq.AutoSize = true;
            this.chkReq.Enabled = false;
            this.chkReq.Location = new System.Drawing.Point(7, 401);
            this.chkReq.Name = "chkReq";
            this.chkReq.Size = new System.Drawing.Size(66, 17);
            this.chkReq.TabIndex = 11;
            this.chkReq.Text = "Request";
            this.chkReq.UseVisualStyleBackColor = true;
            this.chkReq.Visible = false;
            // 
            // gbUserApr
            // 
            this.gbUserApr.Controls.Add(this.txtKdCbgApr);
            this.gbUserApr.Controls.Add(this.txtKdBankApr);
            this.gbUserApr.Controls.Add(this.lblKodeCabangApr);
            this.gbUserApr.Controls.Add(this.lblKodeBankApr);
            this.gbUserApr.Controls.Add(this.lblPwdApr);
            this.gbUserApr.Controls.Add(this.txtPwdAppr);
            this.gbUserApr.Controls.Add(this.txtUserAppr);
            this.gbUserApr.Controls.Add(this.lblUserApr);
            this.gbUserApr.Location = new System.Drawing.Point(2, 150);
            this.gbUserApr.Name = "gbUserApr";
            this.gbUserApr.Size = new System.Drawing.Size(329, 71);
            this.gbUserApr.TabIndex = 3;
            this.gbUserApr.TabStop = false;
            this.gbUserApr.Text = "User Checker Batch";
            // 
            // txtKdCbgApr
            // 
            this.txtKdCbgApr.Location = new System.Drawing.Point(92, 43);
            this.txtKdCbgApr.MaxLength = 5;
            this.txtKdCbgApr.Name = "txtKdCbgApr";
            this.txtKdCbgApr.Size = new System.Drawing.Size(60, 20);
            this.txtKdCbgApr.TabIndex = 5;
            // 
            // txtKdBankApr
            // 
            this.txtKdBankApr.Location = new System.Drawing.Point(92, 18);
            this.txtKdBankApr.MaxLength = 5;
            this.txtKdBankApr.Name = "txtKdBankApr";
            this.txtKdBankApr.Size = new System.Drawing.Size(60, 20);
            this.txtKdBankApr.TabIndex = 4;
            // 
            // lblKodeCabangApr
            // 
            this.lblKodeCabangApr.AutoSize = true;
            this.lblKodeCabangApr.Location = new System.Drawing.Point(17, 46);
            this.lblKodeCabangApr.Name = "lblKodeCabangApr";
            this.lblKodeCabangApr.Size = new System.Drawing.Size(72, 13);
            this.lblKodeCabangApr.TabIndex = 1;
            this.lblKodeCabangApr.Text = "Kode Cabang";
            // 
            // lblKodeBankApr
            // 
            this.lblKodeBankApr.AutoSize = true;
            this.lblKodeBankApr.Location = new System.Drawing.Point(17, 21);
            this.lblKodeBankApr.Name = "lblKodeBankApr";
            this.lblKodeBankApr.Size = new System.Drawing.Size(60, 13);
            this.lblKodeBankApr.TabIndex = 0;
            this.lblKodeBankApr.Text = "Kode Bank";
            // 
            // lblPwdApr
            // 
            this.lblPwdApr.AutoSize = true;
            this.lblPwdApr.Location = new System.Drawing.Point(161, 46);
            this.lblPwdApr.Name = "lblPwdApr";
            this.lblPwdApr.Size = new System.Drawing.Size(53, 13);
            this.lblPwdApr.TabIndex = 3;
            this.lblPwdApr.Text = "Password";
            // 
            // txtPwdAppr
            // 
            this.txtPwdAppr.Enabled = false;
            this.txtPwdAppr.Location = new System.Drawing.Point(221, 43);
            this.txtPwdAppr.MaxLength = 20;
            this.txtPwdAppr.Name = "txtPwdAppr";
            this.txtPwdAppr.PasswordChar = '*';
            this.txtPwdAppr.Size = new System.Drawing.Size(100, 20);
            this.txtPwdAppr.TabIndex = 7;
            // 
            // txtUserAppr
            // 
            this.txtUserAppr.Enabled = false;
            this.txtUserAppr.Location = new System.Drawing.Point(221, 18);
            this.txtUserAppr.MaxLength = 20;
            this.txtUserAppr.Name = "txtUserAppr";
            this.txtUserAppr.Size = new System.Drawing.Size(100, 20);
            this.txtUserAppr.TabIndex = 6;
            // 
            // lblUserApr
            // 
            this.lblUserApr.AutoSize = true;
            this.lblUserApr.Location = new System.Drawing.Point(161, 21);
            this.lblUserApr.Name = "lblUserApr";
            this.lblUserApr.Size = new System.Drawing.Size(29, 13);
            this.lblUserApr.TabIndex = 2;
            this.lblUserApr.Text = "User";
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(395, 391);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Location = new System.Drawing.Point(295, 391);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbConn
            // 
            this.gbConn.Controls.Add(this.lblDBName);
            this.gbConn.Controls.Add(this.txtDBName);
            this.gbConn.Controls.Add(this.lblDBPwd);
            this.gbConn.Controls.Add(this.lblDBUser);
            this.gbConn.Controls.Add(this.lblDBHost);
            this.gbConn.Controls.Add(this.txtDBPwd);
            this.gbConn.Controls.Add(this.txtDBUid);
            this.gbConn.Controls.Add(this.txtDBIP);
            this.gbConn.Location = new System.Drawing.Point(335, 6);
            this.gbConn.Name = "gbConn";
            this.gbConn.Size = new System.Drawing.Size(330, 71);
            this.gbConn.TabIndex = 5;
            this.gbConn.TabStop = false;
            this.gbConn.Text = "Connection";
            // 
            // lblDBName
            // 
            this.lblDBName.AutoSize = true;
            this.lblDBName.Location = new System.Drawing.Point(6, 46);
            this.lblDBName.Name = "lblDBName";
            this.lblDBName.Size = new System.Drawing.Size(53, 13);
            this.lblDBName.TabIndex = 7;
            this.lblDBName.Text = "DB Name";
            // 
            // txtDBName
            // 
            this.txtDBName.Enabled = false;
            this.txtDBName.Location = new System.Drawing.Point(59, 43);
            this.txtDBName.MaxLength = 20;
            this.txtDBName.Name = "txtDBName";
            this.txtDBName.Size = new System.Drawing.Size(90, 20);
            this.txtDBName.TabIndex = 1;
            // 
            // lblDBPwd
            // 
            this.lblDBPwd.AutoSize = true;
            this.lblDBPwd.Location = new System.Drawing.Point(161, 46);
            this.lblDBPwd.Name = "lblDBPwd";
            this.lblDBPwd.Size = new System.Drawing.Size(53, 13);
            this.lblDBPwd.TabIndex = 5;
            this.lblDBPwd.Text = "Password";
            // 
            // lblDBUser
            // 
            this.lblDBUser.AutoSize = true;
            this.lblDBUser.Location = new System.Drawing.Point(161, 21);
            this.lblDBUser.Name = "lblDBUser";
            this.lblDBUser.Size = new System.Drawing.Size(29, 13);
            this.lblDBUser.TabIndex = 4;
            this.lblDBUser.Text = "User";
            // 
            // lblDBHost
            // 
            this.lblDBHost.AutoSize = true;
            this.lblDBHost.Location = new System.Drawing.Point(6, 21);
            this.lblDBHost.Name = "lblDBHost";
            this.lblDBHost.Size = new System.Drawing.Size(47, 13);
            this.lblDBHost.TabIndex = 3;
            this.lblDBHost.Text = "DB Host";
            // 
            // txtDBPwd
            // 
            this.txtDBPwd.Enabled = false;
            this.txtDBPwd.Location = new System.Drawing.Point(215, 42);
            this.txtDBPwd.MaxLength = 20;
            this.txtDBPwd.Name = "txtDBPwd";
            this.txtDBPwd.PasswordChar = '*';
            this.txtDBPwd.Size = new System.Drawing.Size(90, 20);
            this.txtDBPwd.TabIndex = 3;
            // 
            // txtDBUid
            // 
            this.txtDBUid.Enabled = false;
            this.txtDBUid.Location = new System.Drawing.Point(215, 18);
            this.txtDBUid.MaxLength = 20;
            this.txtDBUid.Name = "txtDBUid";
            this.txtDBUid.Size = new System.Drawing.Size(90, 20);
            this.txtDBUid.TabIndex = 2;
            // 
            // txtDBIP
            // 
            this.txtDBIP.Enabled = false;
            this.txtDBIP.Location = new System.Drawing.Point(59, 18);
            this.txtDBIP.MaxLength = 50;
            this.txtDBIP.Name = "txtDBIP";
            this.txtDBIP.Size = new System.Drawing.Size(90, 20);
            this.txtDBIP.TabIndex = 0;
            // 
            // gbUserReq
            // 
            this.gbUserReq.Controls.Add(this.txtKdCbgReq);
            this.gbUserReq.Controls.Add(this.txtKdBankReq);
            this.gbUserReq.Controls.Add(this.lblKodeCabangReq);
            this.gbUserReq.Controls.Add(this.lblKodeBankReq);
            this.gbUserReq.Controls.Add(this.lblPwdReq);
            this.gbUserReq.Controls.Add(this.txtPwdReq);
            this.gbUserReq.Controls.Add(this.txtUserReq);
            this.gbUserReq.Controls.Add(this.lblUserReq);
            this.gbUserReq.Location = new System.Drawing.Point(2, 6);
            this.gbUserReq.Name = "gbUserReq";
            this.gbUserReq.Size = new System.Drawing.Size(329, 71);
            this.gbUserReq.TabIndex = 0;
            this.gbUserReq.TabStop = false;
            this.gbUserReq.Text = "User Maker Batch";
            // 
            // txtKdCbgReq
            // 
            this.txtKdCbgReq.Location = new System.Drawing.Point(92, 43);
            this.txtKdCbgReq.MaxLength = 5;
            this.txtKdCbgReq.Name = "txtKdCbgReq";
            this.txtKdCbgReq.Size = new System.Drawing.Size(60, 20);
            this.txtKdCbgReq.TabIndex = 5;
            this.txtKdCbgReq.TextChanged += new System.EventHandler(this.txtKdCbgReq_TextChanged);
            // 
            // txtKdBankReq
            // 
            this.txtKdBankReq.Location = new System.Drawing.Point(92, 18);
            this.txtKdBankReq.MaxLength = 5;
            this.txtKdBankReq.Name = "txtKdBankReq";
            this.txtKdBankReq.Size = new System.Drawing.Size(60, 20);
            this.txtKdBankReq.TabIndex = 4;
            this.txtKdBankReq.TextChanged += new System.EventHandler(this.txtKdBankReq_TextChanged);
            // 
            // lblKodeCabangReq
            // 
            this.lblKodeCabangReq.AutoSize = true;
            this.lblKodeCabangReq.Location = new System.Drawing.Point(17, 46);
            this.lblKodeCabangReq.Name = "lblKodeCabangReq";
            this.lblKodeCabangReq.Size = new System.Drawing.Size(72, 13);
            this.lblKodeCabangReq.TabIndex = 1;
            this.lblKodeCabangReq.Text = "Kode Cabang";
            // 
            // lblKodeBankReq
            // 
            this.lblKodeBankReq.AutoSize = true;
            this.lblKodeBankReq.Location = new System.Drawing.Point(17, 21);
            this.lblKodeBankReq.Name = "lblKodeBankReq";
            this.lblKodeBankReq.Size = new System.Drawing.Size(60, 13);
            this.lblKodeBankReq.TabIndex = 0;
            this.lblKodeBankReq.Text = "Kode Bank";
            // 
            // lblPwdReq
            // 
            this.lblPwdReq.AutoSize = true;
            this.lblPwdReq.Location = new System.Drawing.Point(161, 46);
            this.lblPwdReq.Name = "lblPwdReq";
            this.lblPwdReq.Size = new System.Drawing.Size(53, 13);
            this.lblPwdReq.TabIndex = 3;
            this.lblPwdReq.Text = "Password";
            // 
            // txtPwdReq
            // 
            this.txtPwdReq.Enabled = false;
            this.txtPwdReq.Location = new System.Drawing.Point(221, 43);
            this.txtPwdReq.MaxLength = 20;
            this.txtPwdReq.Name = "txtPwdReq";
            this.txtPwdReq.PasswordChar = '*';
            this.txtPwdReq.Size = new System.Drawing.Size(100, 20);
            this.txtPwdReq.TabIndex = 7;
            this.txtPwdReq.TextChanged += new System.EventHandler(this.txtPwdReq_TextChanged);
            // 
            // txtUserReq
            // 
            this.txtUserReq.Enabled = false;
            this.txtUserReq.Location = new System.Drawing.Point(221, 18);
            this.txtUserReq.MaxLength = 20;
            this.txtUserReq.Name = "txtUserReq";
            this.txtUserReq.Size = new System.Drawing.Size(100, 20);
            this.txtUserReq.TabIndex = 6;
            this.txtUserReq.TextChanged += new System.EventHandler(this.txtUserReq_TextChanged);
            // 
            // lblUserReq
            // 
            this.lblUserReq.AutoSize = true;
            this.lblUserReq.Location = new System.Drawing.Point(161, 21);
            this.lblUserReq.Name = "lblUserReq";
            this.lblUserReq.Size = new System.Drawing.Size(29, 13);
            this.lblUserReq.TabIndex = 2;
            this.lblUserReq.Text = "User";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(245)))), ((int)(((byte)(241)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(728, 443);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 26);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // OpenDirBatch
            // 
            this.OpenDirBatch.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // lblCurrProgress
            // 
            this.lblCurrProgress.AutoSize = true;
            this.lblCurrProgress.Location = new System.Drawing.Point(115, 459);
            this.lblCurrProgress.Name = "lblCurrProgress";
            this.lblCurrProgress.Size = new System.Drawing.Size(16, 13);
            this.lblCurrProgress.TabIndex = 3;
            this.lblCurrProgress.Text = "...";
            // 
            // pbCurr
            // 
            this.pbCurr.Location = new System.Drawing.Point(117, 443);
            this.pbCurr.Name = "pbCurr";
            this.pbCurr.Size = new System.Drawing.Size(575, 12);
            this.pbCurr.Step = 5;
            this.pbCurr.TabIndex = 4;
            // 
            // lblAddProgress
            // 
            this.lblAddProgress.Location = new System.Drawing.Point(409, 458);
            this.lblAddProgress.Name = "lblAddProgress";
            this.lblAddProgress.Size = new System.Drawing.Size(283, 14);
            this.lblAddProgress.TabIndex = 5;
            this.lblAddProgress.Text = "..";
            this.lblAddProgress.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // _extender
            // 
            this._extender.DataGridView = this.dgvInquiry;
            defaultGridFilterFactory1.CreateDistinctGridFilters = false;
            defaultGridFilterFactory1.DefaultGridFilterType = typeof(ExtendedWinControl.GridFilters.TextGridFilter);
            defaultGridFilterFactory1.DefaultShowDateInBetweenOperator = false;
            defaultGridFilterFactory1.DefaultShowNumericInBetweenOperator = false;
            defaultGridFilterFactory1.HandleEnumerationTypes = true;
            defaultGridFilterFactory1.MaximumDistinctValues = 20;
            this._extender.FilterFactory = defaultGridFilterFactory1;
            this._extender.FilterTextVisible = false;
            // 
            // cbMF
            // 
            this.cbMF.AutoSize = true;
            this.cbMF.Location = new System.Drawing.Point(235, 100);
            this.cbMF.Name = "cbMF";
            this.cbMF.Size = new System.Drawing.Size(41, 17);
            this.cbMF.TabIndex = 15;
            this.cbMF.Text = "MF";
            this.cbMF.UseVisualStyleBackColor = true;
            // 
            // cbCO
            // 
            this.cbCO.AutoSize = true;
            this.cbCO.Location = new System.Drawing.Point(280, 100);
            this.cbCO.Name = "cbCO";
            this.cbCO.Size = new System.Drawing.Size(41, 17);
            this.cbCO.TabIndex = 16;
            this.cbCO.Text = "CO";
            this.cbCO.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 476);
            this.Controls.Add(this.lblAddProgress);
            this.Controls.Add(this.pbCurr);
            this.Controls.Add(this.lblCurrProgress);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.prevTab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SID Batch Automation";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.prevTab.ResumeLayout(false);
            this.tpMain.ResumeLayout(false);
            this.tpMain.PerformLayout();
            this.tpPreview.ResumeLayout(false);
            this.tpRename.ResumeLayout(false);
            this.tpRename.PerformLayout();
            this.tpInq.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInquiry)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gbFilterInq.ResumeLayout(false);
            this.gbFilterInq.PerformLayout();
            this.tpSetting.ResumeLayout(false);
            this.tpSetting.PerformLayout();
            this.gbWebIDB.ResumeLayout(false);
            this.gbWebIDB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxPageIDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxLoopIDB)).EndInit();
            this.gbWebBKM.ResumeLayout(false);
            this.gbWebBKM.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxPageBKM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxLoopBKM)).EndInit();
            this.gbMisc.ResumeLayout(false);
            this.gbMisc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nMinDataGen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nLoop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxTxtGen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nMaxDataGen)).EndInit();
            this.gbTime.ResumeLayout(false);
            this.gbTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nFreqMinute)).EndInit();
            this.gbUserWeb.ResumeLayout(false);
            this.gbUserWeb.PerformLayout();
            this.gbSearchCrit.ResumeLayout(false);
            this.gbSearchCrit.PerformLayout();
            this.gbFolder.ResumeLayout(false);
            this.gbFolder.PerformLayout();
            this.gbUserApr.ResumeLayout(false);
            this.gbUserApr.PerformLayout();
            this.gbConn.ResumeLayout(false);
            this.gbConn.PerformLayout();
            this.gbUserReq.ResumeLayout(false);
            this.gbUserReq.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._extender)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl prevTab;
        private System.Windows.Forms.TabPage tpMain;
        private System.Windows.Forms.TabPage tpSetting;
        private System.Windows.Forms.GroupBox gbUserReq;
        private System.Windows.Forms.Label lblUserReq;
        private System.Windows.Forms.TextBox txtPwdReq;
        private System.Windows.Forms.TextBox txtUserReq;
        private System.Windows.Forms.Label lblPwdReq;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox gbConn;
        private System.Windows.Forms.TextBox txtDBIP;
        private System.Windows.Forms.TextBox txtDBPwd;
        private System.Windows.Forms.TextBox txtDBUid;
        private System.Windows.Forms.Label lblDBPwd;
        private System.Windows.Forms.Label lblDBUser;
        private System.Windows.Forms.Label lblDBHost;
        private System.Windows.Forms.Label lblDBName;
        private System.Windows.Forms.TextBox txtDBName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.FolderBrowserDialog OpenDirBatch;
        private System.Windows.Forms.Timer tmrRealTime;
        private System.Windows.Forms.GroupBox gbUserApr;
        private System.Windows.Forms.Label lblPwdApr;
        private System.Windows.Forms.TextBox txtPwdAppr;
        private System.Windows.Forms.TextBox txtUserAppr;
        private System.Windows.Forms.Label lblUserApr;
        private System.Windows.Forms.CheckBox chkDwn;
        private System.Windows.Forms.CheckBox chkAppr;
        private System.Windows.Forms.CheckBox chkReq;
        private System.Windows.Forms.TabPage tpPreview;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.WebBrowser wbIE;
        private System.Windows.Forms.Label lblKodeCabangReq;
        private System.Windows.Forms.Label lblKodeBankReq;
        private System.Windows.Forms.TextBox txtKdCbgReq;
        private System.Windows.Forms.TextBox txtKdBankReq;
        private System.Windows.Forms.TextBox txtKdCbgApr;
        private System.Windows.Forms.TextBox txtKdBankApr;
        private System.Windows.Forms.Label lblKodeCabangApr;
        private System.Windows.Forms.Label lblKodeBankApr;
        private System.Windows.Forms.GroupBox gbFolder;
        private System.Windows.Forms.Button btnOutput;
        private System.Windows.Forms.Button btnInput;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.TextBox txtInputFolder;
        private System.Windows.Forms.Label lblCurrProgress;
        private System.Windows.Forms.GroupBox gbSearchCrit;
        private System.Windows.Forms.CheckBox cbCritNPWP;
        private System.Windows.Forms.CheckBox cbCritKTP;
        private System.Windows.Forms.CheckBox cbCritGender;
        private System.Windows.Forms.CheckBox cbCritDOB;
        private System.Windows.Forms.CheckBox cbCritName;
        private System.Windows.Forms.CheckBox cbPL;
        private System.Windows.Forms.CheckBox cbCC;
        private System.Windows.Forms.Label lblDtGen;
        private System.Windows.Forms.NumericUpDown nMaxDataGen;
        private System.Windows.Forms.TabPage tpInq;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblInqTo;
        private System.Windows.Forms.Label lblInqDate;
        private System.Windows.Forms.Label lblInqBatName;
        private System.Windows.Forms.Label lblInqStatus;
        private System.Windows.Forms.TextBox txtInqBatName;
        private System.Windows.Forms.Button btnInqExport;
        private System.Windows.Forms.Button btnInqBack;
        private System.Windows.Forms.ProgressBar pbCurr;
        private System.Windows.Forms.Label lblAddProgress;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnInqSearch;
        private System.Windows.Forms.ComboBox cmbInqStatus;
        private System.Windows.Forms.DataGridView dgvInquiry;
        private System.Windows.Forms.DateTimePicker dtpInqTo;
        private System.Windows.Forms.DateTimePicker dtpInqFrom;
        private System.Windows.Forms.Button btnBKM;
        private System.Windows.Forms.TextBox txtBKMFolder;
        private System.Windows.Forms.CheckBox cbResume;
        private System.Windows.Forms.GroupBox gbUserWeb;
        private System.Windows.Forms.CheckBox cbEqBatch;
        private System.Windows.Forms.TextBox txtPwdWeb;
        private System.Windows.Forms.TextBox txtUserWeb;
        private System.Windows.Forms.Label lblPwdWeb;
        private System.Windows.Forms.Label lblUserWeb;
        private System.Windows.Forms.TextBox txtKdCbgWeb;
        private System.Windows.Forms.TextBox txtKdBankWeb;
        private System.Windows.Forms.Label lblKodeCabangWeb;
        private System.Windows.Forms.Label lblKodeBankWeb;
        private System.Windows.Forms.NumericUpDown nMaxTxtGen;
        private System.Windows.Forms.Label lblTxtGen;
        private System.Windows.Forms.GroupBox gbTime;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private ExtendedWinControl.CheckedComboBox ccbDays;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.Label lblsd;
        private System.Windows.Forms.GroupBox gbMisc;
        private System.Windows.Forms.Label lblModule;
        private System.Windows.Forms.NumericUpDown nDelay;
        private System.Windows.Forms.Label lblDelayTime;
        private System.Windows.Forms.NumericUpDown nLoop;
        private System.Windows.Forms.Label lblLoop;
        private System.Windows.Forms.TabPage tpRename;
        private System.Windows.Forms.TextBox tFailedRename;
        private System.Windows.Forms.Label lblTxtDir;
        private System.Windows.Forms.Label lblIDIDir;
        private System.Windows.Forms.Button btnTxtDir;
        private System.Windows.Forms.TextBox txtIDIDir;
        private System.Windows.Forms.TextBox txtTxtDir;
        private System.Windows.Forms.Button btnIDIDir;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblCurrStage;
        private System.Windows.Forms.Label lblMaxPageBKM;
        private System.Windows.Forms.Label lblMaxLoopBKM;
        private System.Windows.Forms.NumericUpDown nMaxPageBKM;
        private System.Windows.Forms.NumericUpDown nMaxLoopBKM;
        private System.Windows.Forms.GroupBox gbWebBKM;
        private System.Windows.Forms.GroupBox gbWebIDB;
        private System.Windows.Forms.NumericUpDown nMaxPageIDB;
        private System.Windows.Forms.Label lblMaxLoopIDB;
        private System.Windows.Forms.NumericUpDown nMaxLoopIDB;
        private System.Windows.Forms.Label lblMaxPageIDB;
        private System.Windows.Forms.Label lblInqTxtName;
        private System.Windows.Forms.TextBox txtInqTxtName;
        private System.Windows.Forms.TextBox txtUIDCBS;
        private System.Windows.Forms.Label lblAppCount;
        private System.Windows.Forms.Button btnUpIDI;
        private System.Windows.Forms.Label lblBatchID;
        private System.Windows.Forms.CheckBox cbAutoConvert;
        private System.Windows.Forms.CheckBox cbLOSInt;
        private System.Windows.Forms.CheckBox cbPrcAll;
        private System.Windows.Forms.TextBox txtInqRegno;
        private System.Windows.Forms.Label lblInqRegno;
        private System.Windows.Forms.Label lblFreqEvery;
        private System.Windows.Forms.Label lblFreqMinute;
        private System.Windows.Forms.NumericUpDown nFreqMinute;
        private System.Windows.Forms.CheckBox cbUseCuType;
        private System.Windows.Forms.TextBox txtInitialLetter;
        private System.Windows.Forms.Label lblInitialLetter;
        private ExtendedWinControl.DataGridFilterExtender _extender;
        private System.Windows.Forms.TextBox txtInqName;
        private System.Windows.Forms.Label lblInqName;
        private System.Windows.Forms.GroupBox gbFilterInq;
        private System.Windows.Forms.TextBox txtFilterName;
        private System.Windows.Forms.TextBox txtFilterRegno;
        private System.Windows.Forms.Label lblFilterName;
        private System.Windows.Forms.Label lblFilterRegno;
        private System.Windows.Forms.NumericUpDown nMinDataGen;
        private System.Windows.Forms.SaveFileDialog dlgSaveExport;
        private System.Windows.Forms.CheckBox cbCO;
        private System.Windows.Forms.CheckBox cbMF;
    }
}

