﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Automation;

namespace SIDBatchDownloader
{
    public class MyClass
    {
        public AutomationElement Root
        {
            get
            {
                return AutomationElement.RootElement;
            }
        }

        public AutomationElement Find(int hwnd)
        {
            return Root.FindFirst(TreeScope.Subtree, CustomConditions.ByHandle(hwnd));
        }

        //public int Find(String className, String name, int index)
        //{
        //    AutomationElementCollection elements = Root.FindAll(TreeScope.Children, CustomConditions.ByName(name));
        //    if (elements.Count <= index)
        //    {
        //        return 0;
        //    }

        //    AutomationElement element = elements[index];

        //    return element.Current.NativeWindowHandle;
        //}

        //public int Find(int parent, String className, String name, int index)
        //{
        //    AutomationElement baseElement = Find(parent);
        //    if (baseElement == null)
        //    {
        //        return 0;
        //    }

        //    AutomationElementCollection elements = baseElement.FindAll(TreeScope.Subtree, CustomConditions.ByName(name));
        //    if (elements.Count <= index)
        //    {
        //        return 0;
        //    }

        //    AutomationElement element = elements[index];

        //    return element.Current.NativeWindowHandle;
        //}

        //public int Find(int parent, int index)
        //{
        //    AutomationElement baseElement = base.Find(parent);
        //    Condition locator = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Tab);
        //    AutomationElementCollection elements = baseElement.FindAll(TreeScope.Subtree, locator);
        //    if (elements.Count <= index)
        //    {
        //        return 0;
        //    }
        //    return elements[index].Current.NativeWindowHandle;
        //}

        public static Condition ByTypeAndName(ControlType type, String name)
        {
            Condition[] locators = 
            { 
                new PropertyCondition(AutomationElement.NameProperty, name),
                new PropertyCondition(AutomationElement.ControlTypeProperty, type)
            };

            return new AndCondition(locators);
        }

        public static Condition ByHandle(int hwnd)
        {
            return new PropertyCondition(AutomationElement.NativeWindowHandleProperty, hwnd);
        }

        public static Condition ByName(String name)
        {
            return new PropertyCondition(AutomationElement.NameProperty, name);
        }

        public string[] GetItemNames(int hwnd)
        {
            List<String> elementNames = new List<String>();

            AutomationElement element = Find(hwnd);
            AutomationElement tabElement = TreeWalker.RawViewWalker.GetFirstChild(element);

            while (tabElement != null)
            {
                elementNames.Add(tabElement.Current.Name);
                tabElement = TreeWalker.RawViewWalker.GetNextSibling(tabElement);
            }

            return elementNames.ToArray();
        }
    }

    public class CustomConditions
    {
        public static Condition ByHandle(int hwnd)
        {
            return new PropertyCondition(AutomationElement.NativeWindowHandleProperty, hwnd);
        }

        public static Condition ByName(String name)
        {
            return new PropertyCondition(AutomationElement.NameProperty, name);
        }

        public static Condition ByTypeAndName(ControlType type, String name)
        {
            Condition[] locators = 
            { 
                new PropertyCondition(AutomationElement.NameProperty, name),
                new PropertyCondition(AutomationElement.ControlTypeProperty, type)
            };

            return new AndCondition(locators);
        }
    }
}
