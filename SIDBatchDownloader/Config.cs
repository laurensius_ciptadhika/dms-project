﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Net;

namespace SIDBatchDownloader
{
    class Config
    {
        #region changelog
        //  Revision History
        // (+) feature added
        // (-) feature removed
        // (*) feature improved / changed
        // (/) bugs fixed
        //
        //  Version number is composed by four digits: application.major.minor.release (example: 1.0.3.18)
        //      Application digit will change only when really big modifications are made. Such modifications would affect the application in deep. 
        //  Read it as a whole application rewriting from scratch, maybe even programming language change. When this digit change, all minor digit will be zeroed. 
        //  This digit will likely never change in the life of this Application.
        //
        //      Major digit will change when major modifications are made. 
        //  Read it as a "new very important functions available" flag. When this digit change, all minor digit will be zeroed.
        //
        //      Minor digit will change when minor modifications or enhancements are made. 
        //  Maybe new useful functions are added but not so important on the usage of the application. 
        //  When this digit change, all minor digit will be zeroed.
        //
        //      Release digit will change when only bug fixes are applied.
        //  So, if bug fixes are applied but also new features are added, the Major or Minor digits will prevail on Release digit.
        //
        //------------------------------- Version History -------------------------------
        //version 1.0.0.0 (skipped)     : LCS - 20131125
        //  + first release
        //
        //version 1.1.0.0               : LCS - 20140130
        //  + multi file processing
        //  + recurrence setting, can select days and time [using CheckedComboBox]
        //  + delay time and loop times can be set
        //  * rename and upload file is manual process, after all IDB extracted by SIDBank
        //  * save fileBatch, fileBKM, fileIDB in database, instead in App.config
        //  * faster loading between screen (using 2 type of method when searching control)
        //  * log files separated by date
        //  * tab control using vertical mode
        //  * send batch merged with import txt
        //  * loop exit SID Batch (closeApp)
        //
        //version 1.1.0.1               : LCS - 20140605
        //  * lower timeout to increase speed
        //  / fix renaming process to third name
        //
        //version 1.1.0.2               : LCS - 20140606
        //  + check screen resolution, it is best used at 1280x1024 pixels
        //  * duplicate data treated as success attempt
        //  - remove popup for testing purpose
        //
        //version 1.1.1.0               : LCS - 20140618
        //  + loop web page and loop times is parameterized
        //  + reporting bkm and idb not found in process
        //  + reporting aplication with no sid
        //  + option to continue if process is finished
        //
        //version 1.1.2.0               : LCS - 20140624
        //  + added USP_MATCHINGIDI and USP_UPLOADIDI_UPDATE to link with APP table
        //  / fix genfilename which is empty
        //  / rollback version, from 1.2.0.0 to 1.1.1.0 because function is not major modification
        //  / fix logic if stopping app when searching IDB would continue to the step process
        //
        //version 1.1.3.0               : LCS - 20140627
        //  + add user ID from CBS to avoid mixed Application from other inbox
        //  / fix renaming not correct, on fourth try skip first name less than 4 character
        //
        //version 1.1.4.0               : LCS - 20140708
        //  * rename method is separated into two method, exact match and similarity
        //  * upload file is separate button
        //  * Rename similar will not delete file, instead copy to be used with other app
        //  / fourth attempt to rename by similarity is commented, because it is not effective
        //  / succesfully renamed similar file will have suffix "x.IDI"
        //
        //version 1.1.4.1               : LCS - 20140717
        //  * when matching BKM is failed (e.g not match), will resume
        //  / fix when upload only look for "x0.IDI" to "*.IDI"
        //  / fix log file Hour from hh to HH (24 format)
        //
        //version 1.1.4.2               : LCS - 20140806
        //  * update method retrieveFileName only get BKM or IDB from last 5 minutes, else it's not generated
        //
        //version 1.1.4.3               : LCS - 20140819
        //  * add one new method to get number of IDB generated
        //
        //version 1.1.4.4               : LCS - 20140829
        //  * fix bug when generate BatchID cause primary key violation
        //
        //version 1.1.4.5               : LCS - 20140917
        //  * show progress when counting IDB created
        //
        //version 1.1.4.6               : LCS - 20140922
        //  * rollback app when not found in BKM
        //  / fix IDI_KREDIT not uploaded when IDIType = 2
        //
        //version 1.1.4.7               : LCS - 20141017
        //  / fix error when clear IDI
        //
        //version 1.1.4.8               : LCS - 20141215
        //  * modify max loop to 5000 and max delay time to 30000
        //  / add looping when click tab header in match IDB
        //
        //version 1.1.4.9               : LCS - 20150821
        //  / change method when read INI SID from AliasName to DatabaseFile
        //
        // version 1.1.5.0              : LCS - 20150930
        // + auto convert checkbox
        // + checkbox user Los Integration
        // + matching IDI when converted
        // + checkbox process all to let app keep running (no dialog continue or not)
        //
        // version 1.1.5.1              : LCS - 20151210
        // + add minimum line per textfile
        //
        //note: please record ANY change to application (and version) into version history!!
        //--------------------------------------------------------------------------------
        #endregion

        #region var declaration
        private static System.Configuration.Configuration config;
        private static string _ConnectionString;
        private static int _ApplicationDelayCheckingInMilliseconds = 2700;
        private static int _dbtimeout;
        private static string pooling;
        private static string _versionInfo = "1.1.5.1";

        // left side
        private static string _kdBankReq;
        private static string _kdCbgReq;
        private static string _UserRq;
        private static string _PwdRq;
        private static bool _EqBatch;
        private static string _kdBankWeb;
        private static string _kdCbgWeb;
        private static string _UserWeb;
        private static string _PwdWeb;
        private static string _kdBankAppr;
        private static string _kdCbgAppr;
        private static string _UserAppr;
        private static string _PwdAppr;
        private static bool _critName;
        private static bool _critDOB;
        private static bool _critGender;
        private static bool _critKTP;
        private static bool _critNPWP;
        private static bool _useCuType;
        private static bool _modCC;
        private static bool _modPL;
        private static bool _LOSInt;
        private static string _UIDCBS;
        private static string _initLetter;
        private static int _BKMLoop;
        private static int _BKMPage;
        private static bool _autoConvert;

        // right side
        private static string dbip;
        private static string dbname;
        private static string uid;
        private static string pwd;
        private static string _inputFolderBatch;
        private static string _BKMFolder;
        private static string _outputFolderBatch;
        private static int _minDataGen;
        private static int _dataGen;
        private static int _txtGen;
        private static bool _isResumeable;
        private static string _days;
        private static string _timeStart;
        private static string _timeEnd;
        private static int _freqMinute;
        private static int _delayMs;
        private static int _loop;
        private static int _IDBLoop;
        private static int _IDBPage;
        

        // Log File Setting
        private static bool _isLogged = false;
        private static string _logFileName = "";
        private static string _logFolder = "";

        // Web Setting
        private static string _urlWebSIDLogin;
        private static string _urlWebSIDLogout;
        private static string _urlWebSIDDomain;
        private static string _urlWebSIDIDI;
        private static string _urlWebSIDDownloadListBatKrm;
        private static string _urlWebSIDDownloadListBat;
        private static string _urlWebSIDCekLogin;
        private static string _urlWebSIDSessionExpired;
        private static string _urlWebSIDLoginFailed;

        // Auto Resume Setting
        private static string _lastAction;
        private static string _fileBatch;
        private static int _batchCount;
        private static string _fileBKM;
        private static string _fileIDB;
        private static int _IDBCounter;
        private static string _batchID;
        private static int _webLoop;
        private static string _lastProcTime;

        // IDI Viewer Setting
        private static string _userIDI;
        private static string _pwdIDI;
        #endregion

        public enum LogType : int
        {
            LOG = 0,
            ERR_APP = 1,
            ERR_WB = 2,
            ERR_SID = 3
        }

        #region setter getter
        public static bool Pooling
        {
            get { try { return bool.Parse(pooling); } catch { return false; } }
            set { pooling = value.ToString(); }
        }

        public static string ConnectionString
        {
            get
            {
                _ConnectionString = "Data Source=" + DataSource +
                    "; Initial Catalog=" + InitialCatalog +
                    "; uid=" + DBUserID +
                    "; pwd=" + DBUserPWD +
                    "; pooling=" + Pooling.ToString();
                return _ConnectionString;
            }
        }

        public static int ApplicationDelayCheckingInMilliseconds
        {
            get { return _ApplicationDelayCheckingInMilliseconds; }
            set { _ApplicationDelayCheckingInMilliseconds = value; }
        }

        public static string AppVersion
        {
            get { return "v" + _versionInfo; }
        }

        // left side configurable
        #region left side
        public static string KdBankReq
        {
            get { return _kdBankReq; }
            set { _kdBankReq = value; }
        }

        public static string KdCbgReq
        {
            get { return _kdCbgReq; }
            set { _kdCbgReq = value; }
        }

        public static string UserRq
        {
            get { return _UserRq; }
            set { _UserRq = value; }
        }

        public static string PwdRq
        {
            get { return decryptPwd(_PwdRq); }
            set { _PwdRq = encryptPwd(value); }
        }

        public static bool EqBatch
        {
            get { return _EqBatch; }
            set { _EqBatch = value; }
        }

        public static string KdBankWeb
        {
            get { return _kdBankWeb; }
            set { _kdBankWeb = value; }
        }

        public static string KdCbgWeb
        {
            get { return _kdCbgWeb; }
            set { _kdCbgWeb = value; }
        }

        public static string UserWeb
        {
            get { return _UserWeb; }
            set { _UserWeb = value; }
        }

        public static string PwdWeb
        {
            get { return decryptPwd(_PwdWeb); }
            set { _PwdWeb = encryptPwd(value); }
        }

        public static string KdBankAppr
        {
            get { return _kdBankAppr; }
            set { _kdBankAppr = value; }
        }

        public static string KdCbgAppr
        {
            get { return _kdCbgAppr; }
            set { _kdCbgAppr = value; }
        }

        public static string UserAppr
        {
            get { return _UserAppr; }
            set { _UserAppr = value; }
        }

        public static string PwdAppr
        {
            get { return decryptPwd(_PwdAppr); }
            set { _PwdAppr = encryptPwd(value); }
        }

        public static bool critName
        {
            get { return _critName; }
            set { _critName = value; }
        }

        public static bool critDOB
        {
            get { return _critDOB; }
            set { _critDOB = value; }
        }

        public static bool critGender
        {
            get { return _critGender; }
            set { _critGender = value; }
        }

        public static bool critKTP
        {
            get { return _critKTP; }
            set { _critKTP = value; }
        }

        public static bool critNPWP
        {
            get { return _critNPWP; }
            set { _critNPWP = value; }
        }

        public static bool useCuType 
        {
            get { return _useCuType; }
            set { _useCuType = value; }
        }

        public static bool modCC
        {
            get { return _modCC; }
            set { _modCC = value; }
        }

        public static bool modPL
        {
            get { return _modPL; }
            set { _modPL = value; }
        }

        public static bool LOSInt 
        {
            get { return _LOSInt; }
            set { _LOSInt = value; }
        }

        public static string UIDCBS
        {
            get { return _UIDCBS; }
            set { _UIDCBS = value; }
        }

        public static string initLetter 
        {
            get { return _initLetter; }
            set { _initLetter = value; }
        }

        public static int BKMLoop
        {
            get { return _BKMLoop; }
            set { _BKMLoop = value; }
        }

        public static int BKMPage
        {
            get { return _BKMPage; }
            set { _BKMPage = value; }
        }

        public static bool autoConvert
        {
            get { return _autoConvert; }
            set { _autoConvert = value; }
        }
        #endregion

        // right side configurable
        #region right side
        public static string DataSource
        {
            get { return dbip; }
            set { dbip = value; }
        }

        public static string InitialCatalog
        {
            get { return dbname; }
            set { dbname = value; }
        }

        public static string DBUserID
        {
            get { return uid; }
            set { uid = value; }
        }

        public static string DBUserPWD
        {
            get { return decryptPwd(pwd); }
            set { pwd = encryptPwd(value); }
        }

        public static string InputFolderBatch
        {
            get { return _inputFolderBatch; }
            set { _inputFolderBatch = value; }
        }

        public static string BKMFolder
        {
            get { return _BKMFolder; }
            set { _BKMFolder = value; }
        }

        public static string OutputFolderBatch
        {
            get { return _outputFolderBatch; }
            set { _outputFolderBatch = value; }
        }

        public static int minDataGen
        {
            get { return _minDataGen; }
            set { _minDataGen = value; }
        }

        public static int dataGen
        {
            get { return _dataGen; }
            set { _dataGen = value; }
        }

        public static int txtGen
        {
            get { return _txtGen; }
            set { _txtGen = value; }
        }

        public static bool isResumeable
        {
            get { return _isResumeable; }
            set { _isResumeable = value; }
        }

        public static string days
        {
            get { return _days; }
            set { _days = value; }
        }

        public static string timeStart
        {
            get { return _timeStart; }
            set { _timeStart = value; }
        }

        public static string timeEnd
        {
            get { return _timeEnd; }
            set { _timeEnd = value; }
        }

        public static int freqMinute
        {
            get { return _freqMinute; }
            set { _freqMinute = value; }
        }

        public static int delayMs
        {
            get { return _delayMs; }
            set { _delayMs = value; }
        }

        public static int loop
        {
            get { return _loop; }
            set { _loop = value; }
        }

        public static int IDBLoop
        {
            get { return _IDBLoop; }
            set { _IDBLoop = value; }
        }

        public static int IDBPage
        {
            get { return _IDBPage; }
            set { _IDBPage = value; }
        }
        #endregion

        #region non configurable
        // log file
        private static bool isLogged
        {
            get { return _isLogged; }
            set { _isLogged = value; }
        }

        private static string logFileName
        {
            get { return _logFileName; }
            set { _logFileName = value; }
        }

        private static string logFolder
        {
            get { return _logFolder; }
            set { _logFolder = value; }
        }

        // web setting
        public static string urlWebSIDLogin
        {
            get { return _urlWebSIDLogin; }
            set { _urlWebSIDLogin = value; }
        }

        public static string urlWebSIDLogout
        {
            get { return _urlWebSIDLogout; }
            set { _urlWebSIDLogout = value; }
        }

        public static string urlWebSIDDomain
        {
            get { return _urlWebSIDDomain; }
            set { _urlWebSIDDomain = value; }
        }

        public static string urlWebSIDIDI
        {
            get { return _urlWebSIDIDI; }
            set { _urlWebSIDIDI = value; }
        }

        public static string urlWebSIDDownloadListBatKrm
        {
            get { return _urlWebSIDDownloadListBatKrm; }
            set { _urlWebSIDDownloadListBatKrm = value; }
        }

        public static string urlWebSIDDownloadListBat
        {
            get { return _urlWebSIDDownloadListBat; }
            set { _urlWebSIDDownloadListBat = value; }
        }

        public static string urlWebSIDCekLogin
        {
            get { return _urlWebSIDCekLogin; }
            set { _urlWebSIDCekLogin = value; }
        }

        public static string urlWebSIDSessionExpired
        {
            get { return _urlWebSIDSessionExpired; }
            set { _urlWebSIDSessionExpired = value; }
        }

        public static string urlWebSIDLoginFailed
        {
            get { return _urlWebSIDLoginFailed; }
            set { _urlWebSIDLoginFailed = value; }
        }

        // auto resume setting
        public static string lastAction
        {
            get { return isResumeable == true ? _lastAction : ""; }
            set { _lastAction = isResumeable == true ? value : ""; }
        }

        public static string fileBatch
        {
            get { return isResumeable == true ? _fileBatch : ""; }
            set { _fileBatch = isResumeable == true ? value : ""; }
        }

        public static int batchCount
        {
            get { return isResumeable == true ? _batchCount : 0; }
            set { _batchCount = isResumeable == true ? value : 0; }
        }

        public static string fileBKM
        {
            get { return isResumeable == true ? _fileBKM : ""; }
            set { _fileBKM = isResumeable == true ? value : ""; }
        }

        public static string fileIDB
        {
            get { return isResumeable == true ? fileIDB : ""; }
            set { fileIDB = isResumeable == true ? value : ""; }
        }

        public static int IDBCounter
        {
            get { return isResumeable == true ? _IDBCounter : 0; }
            set { _IDBCounter = isResumeable == true ? value : 0; }
        }

        public static string batchID
        {
            get { return isResumeable == true ? _batchID : ""; }
            set { _batchID = isResumeable == true ? value : ""; }
        }

        public static int webLoop 
        {
            get { return isResumeable == true ? _webLoop : 0; }
            set { _webLoop = isResumeable == true ? value : 0; }
        }

        public static string lastProcTime
        {
            get { return isResumeable == true ? _lastProcTime : ""; }
            set { _lastProcTime = isResumeable == true ? value : ""; }
        }

        // sid viewer setting
        public static string userIDI
        {
            get { return _userIDI; }
            set { _userIDI = value; }
        }

        public static string pwdIDI
        {
            get { return _pwdIDI; }
            set { _pwdIDI = value; }
        }
        #endregion

        #endregion

        #region function
        public static string decryptPwd(string encryptedString)
        {
            if (encryptedString.Trim().Length < 3)
            {
                return "";
            }
            string tmpResult, decpwd = "";
            for (int i = 2; i < encryptedString.Length; i++)
            {
                char chr = (char)(encryptedString[i] - 2);
                decpwd += new string(chr, 1);
            }
            tmpResult = decpwd;
            return tmpResult;
        }

        public static string encryptPwd(string StringWantToEncrypt)
        {
            string tmpResult, encpwd = "";
            for (int i = 0; i < StringWantToEncrypt.Length; i++)
            {
                char chr = (char)(StringWantToEncrypt[i] + 2);
                encpwd += new string(chr, 1);
            }
            tmpResult = "fg" + encpwd;
            return tmpResult;
        }

        public static bool SaveToConfig()
        {
            bool result = false;
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                config.AppSettings.Settings["KdBankReq"].Value = _kdBankReq;
                config.AppSettings.Settings["KdCbgReq"].Value = _kdCbgReq;
                config.AppSettings.Settings["UserReq"].Value = _UserRq;
                config.AppSettings.Settings["PwdReq"].Value = _PwdRq;
                config.AppSettings.Settings["EqBatch"].Value = _EqBatch == true ? "1" : "0";
                config.AppSettings.Settings["KdBankWeb"].Value = _kdBankWeb;
                config.AppSettings.Settings["KdCbgWeb"].Value = _kdCbgWeb;
                config.AppSettings.Settings["UserWeb"].Value = _UserWeb;
                config.AppSettings.Settings["PwdWeb"].Value = _PwdWeb;
                config.AppSettings.Settings["KdBankAppr"].Value = _kdBankAppr;
                config.AppSettings.Settings["KdCbgAppr"].Value = _kdCbgAppr;
                config.AppSettings.Settings["UserAppr"].Value = @_UserAppr;
                config.AppSettings.Settings["PwdAppr"].Value = @_PwdAppr;
                config.AppSettings.Settings["critName"].Value = _critName == true ? "1" : "0";
                config.AppSettings.Settings["critDOB"].Value = _critDOB == true ? "1" : "0";
                config.AppSettings.Settings["critGender"].Value = _critGender == true ? "1" : "0";
                config.AppSettings.Settings["critKTP"].Value = _critKTP == true ? "1" : "0";
                config.AppSettings.Settings["critNPWP"].Value = _critNPWP == true ? "1" : "0";
                config.AppSettings.Settings["useCuType"].Value = _useCuType == true ? "1" : "0";
                config.AppSettings.Settings["modCC"].Value = _modCC == true ? "1" : "0";
                config.AppSettings.Settings["modPL"].Value = _modPL == true ? "1" : "0";
                config.AppSettings.Settings["LOSInt"].Value = _LOSInt == true ? "1" : "0";
                config.AppSettings.Settings["UIDCBS"].Value = _UIDCBS;
                config.AppSettings.Settings["initLetter"].Value = _initLetter;
                config.AppSettings.Settings["BKMLoop"].Value = _BKMLoop.ToString();
                config.AppSettings.Settings["BKMPage"].Value = _BKMPage.ToString();
                config.AppSettings.Settings["AutoConvert"].Value = _autoConvert == true ? "1" : "0";

                config.AppSettings.Settings["IP"].Value = dbip;
                config.AppSettings.Settings["DBName"].Value = dbname;
                config.AppSettings.Settings["DBUserId"].Value = uid;
                config.AppSettings.Settings["DBPwd"].Value = pwd;
                config.AppSettings.Settings["InputFolderBatch"].Value = _inputFolderBatch;
                config.AppSettings.Settings["BKMFolder"].Value = _BKMFolder;
                config.AppSettings.Settings["OutputFolderBatch"].Value = _outputFolderBatch;
                config.AppSettings.Settings["minDataGen"].Value = _minDataGen.ToString();
                config.AppSettings.Settings["dataGen"].Value = _dataGen.ToString();
                config.AppSettings.Settings["txtGen"].Value = _txtGen.ToString();
                config.AppSettings.Settings["isResumeable"].Value = _isResumeable == true ? "1" : "0";
                config.AppSettings.Settings["days"].Value = _days;
                config.AppSettings.Settings["timeStart"].Value = _timeStart;
                config.AppSettings.Settings["timeEnd"].Value = _timeEnd;
                config.AppSettings.Settings["freqMinute"].Value = _freqMinute.ToString();
                config.AppSettings.Settings["delayMs"].Value = _delayMs.ToString();
                config.AppSettings.Settings["loop"].Value = _loop.ToString();
                config.AppSettings.Settings["IDBLoop"].Value = _IDBLoop.ToString();
                config.AppSettings.Settings["IDBPage"].Value = _IDBPage.ToString();

                config.AppSettings.Settings["isLogged"].Value = isLogged == true ? "1" : "0";
                config.AppSettings.Settings["logFileName"].Value = logFileName;
                config.AppSettings.Settings["logFolder"].Value = logFolder;
                config.AppSettings.Settings["ApplicationDelayCheckingInMilliseconds"].Value = _ApplicationDelayCheckingInMilliseconds.ToString();

                config.AppSettings.Settings["urlWebSIDLogin"].Value = _urlWebSIDLogin;
                config.AppSettings.Settings["urlWebSIDLogout"].Value = _urlWebSIDLogout;
                config.AppSettings.Settings["urlWebSIDDomain"].Value = _urlWebSIDDomain;
                config.AppSettings.Settings["urlWebSIDIDI"].Value = _urlWebSIDIDI;
                config.AppSettings.Settings["urlWebSIDDownloadListBatKrm"].Value = _urlWebSIDDownloadListBatKrm;
                config.AppSettings.Settings["urlWebSIDDownloadListBat"].Value = _urlWebSIDDownloadListBat;
                config.AppSettings.Settings["urlWebSIDCekLogin"].Value = _urlWebSIDCekLogin;
                config.AppSettings.Settings["urlWebSIDSessionExpired"].Value = _urlWebSIDSessionExpired;
                config.AppSettings.Settings["urlWebSIDLoginFailed"].Value = _urlWebSIDLoginFailed;

                //config.app
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;
                LoadConfig();
                result = true;
            }
            catch
            { }
            return result;
        }

        public static void LoadConfig()
        {
            if (ConfigurationSettings.AppSettings["KdBankReq"] != null)
                _kdBankReq = ConfigurationSettings.AppSettings["KdBankReq"];
            if (ConfigurationSettings.AppSettings["KdCbgReq"] != null)
                _kdCbgReq = ConfigurationSettings.AppSettings["KdCbgReq"];
            if (ConfigurationSettings.AppSettings["UserReq"] != null)
                _UserRq = ConfigurationSettings.AppSettings["UserReq"];
            if (ConfigurationSettings.AppSettings["PwdReq"] != null)
                _PwdRq = ConfigurationSettings.AppSettings["PwdReq"];
            if (ConfigurationSettings.AppSettings["EqBatch"] != null)
                _EqBatch = ConfigurationSettings.AppSettings["EqBatch"] == "1";
            if (ConfigurationSettings.AppSettings["KdBankWeb"] != null)
                _kdBankWeb = ConfigurationSettings.AppSettings["KdBankWeb"];
            if (ConfigurationSettings.AppSettings["KdCbgWeb"] != null)
                _kdCbgWeb = ConfigurationSettings.AppSettings["KdCbgWeb"];
            if (ConfigurationSettings.AppSettings["UserWeb"] != null)
                _UserWeb = ConfigurationSettings.AppSettings["UserWeb"];
            if (ConfigurationSettings.AppSettings["PwdWeb"] != null)
                _PwdWeb = ConfigurationSettings.AppSettings["PwdWeb"];
            if (ConfigurationSettings.AppSettings["KdBankAppr"] != null)
                _kdBankAppr = ConfigurationSettings.AppSettings["KdBankAppr"];
            if (ConfigurationSettings.AppSettings["KdCbgAppr"] != null)
                _kdCbgAppr = ConfigurationSettings.AppSettings["KdCbgAppr"];
            if (ConfigurationSettings.AppSettings["UserAppr"] != null)
                _UserAppr = ConfigurationSettings.AppSettings["UserAppr"];
            if (ConfigurationSettings.AppSettings["PwdAppr"] != null)
                _PwdAppr = ConfigurationSettings.AppSettings["PwdAppr"];
            if (ConfigurationSettings.AppSettings["critName"] != null)
                _critName = ConfigurationSettings.AppSettings["critName"] == "1";
            if (ConfigurationSettings.AppSettings["critDOB"] != null)
                _critDOB = ConfigurationSettings.AppSettings["critDOB"] == "1";
            if (ConfigurationSettings.AppSettings["critGender"] != null)
                _critGender = ConfigurationSettings.AppSettings["critGender"] == "1";
            if (ConfigurationSettings.AppSettings["critKTP"] != null)
                _critKTP = ConfigurationSettings.AppSettings["critKTP"] == "1";
            if (ConfigurationSettings.AppSettings["critNPWP"] != null)
                _critNPWP = ConfigurationSettings.AppSettings["critNPWP"] == "1";
            if (ConfigurationSettings.AppSettings["useCuType"] != null)
                _useCuType = ConfigurationSettings.AppSettings["useCuType"] == "1";
            if (ConfigurationSettings.AppSettings["modCC"] != null)
                _modCC = ConfigurationSettings.AppSettings["modCC"] == "1";
            if (ConfigurationSettings.AppSettings["modPL"] != null)
                _modPL = ConfigurationSettings.AppSettings["modPL"] == "1";
            if (ConfigurationSettings.AppSettings["LOSInt"] != null)
                _LOSInt = ConfigurationSettings.AppSettings["LOSInt"] == "1";
            if (ConfigurationSettings.AppSettings["UIDCBS"] != null)
                _UIDCBS = ConfigurationSettings.AppSettings["UIDCBS"];
            if (ConfigurationSettings.AppSettings["initLetter"] != null)
                _initLetter = ConfigurationSettings.AppSettings["initLetter"];
            if (ConfigurationSettings.AppSettings["BKMLoop"] != null)
                _BKMLoop = int.Parse(ConfigurationSettings.AppSettings["BKMLoop"]);
            if (ConfigurationSettings.AppSettings["BKMPage"] != null)
                _BKMPage = int.Parse(ConfigurationSettings.AppSettings["BKMPage"]);
            if (ConfigurationSettings.AppSettings["AutoConvert"] != null)
                _autoConvert = ConfigurationSettings.AppSettings["AutoConvert"] == "1";

            if (ConfigurationSettings.AppSettings["IP"] != null)
                dbip = ConfigurationSettings.AppSettings["IP"];
            if (ConfigurationSettings.AppSettings["DBUserId"] != null)
                uid = ConfigurationSettings.AppSettings["DBUserId"];
            if (ConfigurationSettings.AppSettings["DBPwd"] != null)
                pwd = ConfigurationSettings.AppSettings["DBPwd"];
            if (ConfigurationSettings.AppSettings["DBName"] != null)
                dbname = ConfigurationSettings.AppSettings["DBName"];
            if (ConfigurationSettings.AppSettings["inputFolderBatch"] != null)
                _inputFolderBatch = ConfigurationSettings.AppSettings["inputFolderBatch"];
            if (ConfigurationSettings.AppSettings["BKMFolder"] != null)
                _BKMFolder = ConfigurationSettings.AppSettings["BKMFolder"];
            if (ConfigurationSettings.AppSettings["outputFolderBatch"] != null)
                _outputFolderBatch = ConfigurationSettings.AppSettings["outputFolderBatch"];
            if (ConfigurationSettings.AppSettings["minDataGen"] != null)
                _minDataGen = int.Parse(ConfigurationSettings.AppSettings["minDataGen"]);
            if (ConfigurationSettings.AppSettings["dataGen"] != null)
                _dataGen = int.Parse(ConfigurationSettings.AppSettings["dataGen"]);
            if (ConfigurationSettings.AppSettings["txtGen"] != null)
                _txtGen = int.Parse(ConfigurationSettings.AppSettings["txtGen"]);
            if (ConfigurationSettings.AppSettings["isResumeable"] != null)
                _isResumeable = ConfigurationSettings.AppSettings["isResumeable"] == "1";
            if (ConfigurationSettings.AppSettings["days"] != null)
                _days = ConfigurationSettings.AppSettings["days"];
            if (ConfigurationSettings.AppSettings["timeStart"] != null)
                _timeStart = ConfigurationSettings.AppSettings["timeStart"];
            if (ConfigurationSettings.AppSettings["timeEnd"] != null)
                _timeEnd = ConfigurationSettings.AppSettings["timeEnd"];
            if (ConfigurationSettings.AppSettings["freqMinute"] != null)
                _freqMinute = int.Parse(ConfigurationSettings.AppSettings["freqMinute"]);
            if (ConfigurationSettings.AppSettings["delayMs"] != null)
                _delayMs = int.Parse(ConfigurationSettings.AppSettings["delayMs"]);
            if (ConfigurationSettings.AppSettings["loop"] != null)
                _loop = int.Parse(ConfigurationSettings.AppSettings["loop"]);
            if (ConfigurationSettings.AppSettings["IDBLoop"] != null)
                _IDBLoop = int.Parse(ConfigurationSettings.AppSettings["IDBLoop"]);
            if (ConfigurationSettings.AppSettings["IDBPage"] != null)
                _IDBPage = int.Parse(ConfigurationSettings.AppSettings["IDBPage"]);

            if (ConfigurationSettings.AppSettings["isLogged"] != null)
                _isLogged = ConfigurationSettings.AppSettings["isLogged"] == "1";
            if (ConfigurationSettings.AppSettings["logFileName"] != null)
                _logFileName = ConfigurationSettings.AppSettings["logFileName"];
            if (ConfigurationSettings.AppSettings["logFolder"] != null)
                _logFolder = ConfigurationSettings.AppSettings["logFolder"];
            if (ConfigurationSettings.AppSettings["ApplicationDelayCheckingInMilliseconds"] != null)
                _ApplicationDelayCheckingInMilliseconds = int.Parse(ConfigurationSettings.AppSettings["ApplicationDelayCheckingInMilliseconds"]);

            if (ConfigurationSettings.AppSettings["urlWebSIDLogin"] != null)
                _urlWebSIDLogin = ConfigurationSettings.AppSettings["urlWebSIDLogin"];
            if (ConfigurationSettings.AppSettings["urlWebSIDLogout"] != null)
                _urlWebSIDLogout = ConfigurationSettings.AppSettings["urlWebSIDLogout"];
            if (ConfigurationSettings.AppSettings["urlWebSIDDomain"] != null)
                _urlWebSIDDomain = ConfigurationSettings.AppSettings["urlWebSIDDomain"];
            if (ConfigurationSettings.AppSettings["urlWebSIDIDI"] != null)
                _urlWebSIDIDI = ConfigurationSettings.AppSettings["urlWebSIDIDI"];
            if (ConfigurationSettings.AppSettings["urlWebSIDDownloadListBatKrm"] != null)
                _urlWebSIDDownloadListBatKrm = ConfigurationSettings.AppSettings["urlWebSIDDownloadListBatKrm"];
            if (ConfigurationSettings.AppSettings["urlWebSIDDownloadListBat"] != null)
                _urlWebSIDDownloadListBat = ConfigurationSettings.AppSettings["urlWebSIDDownloadListBat"];
            if (ConfigurationSettings.AppSettings["urlWebSIDCekLogin"] != null)
                _urlWebSIDCekLogin = ConfigurationSettings.AppSettings["urlWebSIDCekLogin"];
            if (ConfigurationSettings.AppSettings["urlWebSIDSessionExpired"] != null)
                _urlWebSIDSessionExpired = ConfigurationSettings.AppSettings["urlWebSIDSessionExpired"];
            if (ConfigurationSettings.AppSettings["urlWebSIDLoginFailed"] != null)
                _urlWebSIDLoginFailed = ConfigurationSettings.AppSettings["urlWebSIDLoginFailed"];

            if (ConfigurationSettings.AppSettings["lastAction"] != null)
                _lastAction = ConfigurationSettings.AppSettings["lastAction"];
            if (ConfigurationSettings.AppSettings["fileBatch"] != null)
                _fileBatch = ConfigurationSettings.AppSettings["fileBatch"];
            if (ConfigurationSettings.AppSettings["batchCount"] != null)
                _batchCount = int.Parse(ConfigurationSettings.AppSettings["batchCount"]);
            if (ConfigurationSettings.AppSettings["fileBKM"] != null)
                _fileBKM = ConfigurationSettings.AppSettings["fileBKM"];
            if (ConfigurationSettings.AppSettings["IDBCounter"] != null)
                _IDBCounter = int.Parse(ConfigurationSettings.AppSettings["IDBCounter"]);
            if (ConfigurationSettings.AppSettings["batchID"] != null)
                _batchID = ConfigurationSettings.AppSettings["batchID"];
            if (ConfigurationSettings.AppSettings["webLoop"] != null)
                _webLoop = int.Parse(ConfigurationSettings.AppSettings["webLoop"]);

            if (ConfigurationSettings.AppSettings["userIDI"] != null)
                _userIDI = ConfigurationSettings.AppSettings["userIDI"];
            if (ConfigurationSettings.AppSettings["pwdIDI"] != null)
                _pwdIDI = ConfigurationSettings.AppSettings["pwdIDI"];
        }

        public static void writeLog(string txt)
        {
            if (_isLogged)
            {
                string fld = System.Windows.Forms.Application.StartupPath + "\\" + logFolder;
                if (!System.IO.Directory.Exists(fld))
                {
                    System.IO.Directory.CreateDirectory(fld);
                }

                string fullPathLog = fld + "\\" + DateTime.Now.ToString("yyyyMMdd") + "_" + logFileName;

                //if (!File.Exists(fullPathLog))
                //    File.Create(fullPathLog);

                using (StreamWriter writer = new StreamWriter(fullPathLog, true))
                {
                    writer.WriteLine(txt);
                    writer.Flush();
                    writer.Close();
                }
            }
        }

        public static void writeLog(string txt, LogType l)
        {
            if (_isLogged)
            {
                string fld = System.Windows.Forms.Application.StartupPath + "\\" + logFolder;
                if (!System.IO.Directory.Exists(fld))
                {
                    System.IO.Directory.CreateDirectory(fld);
                }

                string fullPathLog = fld + "\\" + DateTime.Now.ToString("yyyyMMdd") + "_" + logFileName;

                string datetime, host, logTxt;
                host = Dns.GetHostName();
                datetime = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                logTxt = "[" + host + "] [" + datetime + "] ";
                switch (l)
                {
                    case LogType.LOG:
                        logTxt += "[LOG]";
                        break;
                    case LogType.ERR_APP:
                        logTxt += "[APP ERROR]";
                        break;
                    case LogType.ERR_WB:
                        logTxt += "[WEB ERROR]";
                        break;
                    case LogType.ERR_SID:
                        logTxt += "[SID ERROR]";
                        break;
                    default:
                        break;
                }
                logTxt += " " + txt;
                
                using (StreamWriter writer = new StreamWriter(fullPathLog, true))
                {
                    writer.WriteLine(logTxt);
                    writer.Flush();
                    writer.Close();
                }
            }
        }

        public static void writeLine(string folder, string file, string txt)
        {
            if (!System.IO.Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }

            //if (!File.Exists(folder + "\\" + file))
            //    File.Create(folder + "\\" + file);


            using (StreamWriter writer = new StreamWriter(folder + "\\" + file, true))
            {
                writer.WriteLine(txt);
                writer.Flush();
                writer.Close();
            }
        }

        public static void logAction(string action)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["lastAction"].Value = isResumeable == true ? action.ToString() : "";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["lastAction"] != null)
                    _lastAction = ConfigurationSettings.AppSettings["lastAction"];
            }
            catch (Exception ex)
            {
                writeLog(ex.Message, LogType.ERR_APP);
            }
        }

        public static void saveLastBatch(string fileName)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["fileBatch"].Value = isResumeable == true ? fileName : "";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["fileBatch"] != null)
                    _fileBatch = ConfigurationSettings.AppSettings["fileBatch"];
            }
            catch { }
        }

        public static void saveBatchCount(int counter)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["batchCount"].Value = isResumeable == true ? counter.ToString() : "0";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["batchCount"] != null)
                    _batchCount = int.Parse(ConfigurationSettings.AppSettings["batchCount"]);
            }
            catch { }
        }

        public static void saveLastBKM(string fileName)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["fileBKM"].Value = isResumeable == true ? fileName : "";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["fileBKM"] != null)
                    _fileBKM = ConfigurationSettings.AppSettings["fileBKM"];
            }
            catch { }
        }

        public static void saveLastIDB(string fileName)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["fileIDB"].Value = isResumeable == true ? fileName : "";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["fileIDB"] != null)
                    _fileIDB = ConfigurationSettings.AppSettings["fileIDB"];
            }
            catch { }
        }

        public static void saveIDBCounter(int counter)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["IDBCounter"].Value = isResumeable == true ? counter.ToString() : "0";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["IDBCounter"] != null)
                    _IDBCounter = int.Parse(ConfigurationSettings.AppSettings["IDBCounter"]);
            }
            catch { }
        }

        public static void saveBatchID(string batchName)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["batchID"].Value = isResumeable == true ? batchName : "";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["batchID"] != null)
                    _batchID = ConfigurationSettings.AppSettings["batchID"];
            }
            catch { }
        }

        public static void saveWebLoop(int loop)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["webLoop"].Value = isResumeable == true ? loop.ToString() : "0";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["webLoop"] != null)
                    _webLoop = int.Parse(ConfigurationSettings.AppSettings["webLoop"]);
            }
            catch { }
        }

        public static void loadLastProcTime()
        {
            if (ConfigurationSettings.AppSettings["lastProcTime"] != null)
                _lastProcTime = ConfigurationSettings.AppSettings["lastProcTime"];
        }

        public static void saveLastProcTime(string time)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["lastProcTime"].Value = isResumeable == true ? time : "";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                config = null;

                if (ConfigurationSettings.AppSettings["lastProcTime"] != null)
                    _lastProcTime = ConfigurationSettings.AppSettings["lastProcTime"];
            }
            catch { }
        }
        #endregion
    }
}
