﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIDBatchDownloader
{
    public partial class frmValidation : Form
    {
        public string SetText;
        public string Input;

        public frmValidation()
        {
            InitializeComponent();
        }

        private void frmValidation_Load(object sender, EventArgs e)
        {
            lblText.Text = SetText;
            txtInput.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Input = txtInput.Text.Trim();
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Input = "";
            this.Hide();
        }

        private void KeyUpx(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOK_Click(null, null);
        }
    }
}
