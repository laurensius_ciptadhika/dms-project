﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Automation;
using System.Windows.Automation.Text;
using System.IO;
using System.Reflection;
using mshtml;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace SIDBatchDownloader
{
    class myFunctions
    {
        private static int loop = Config.loop;
        private static int loopTimeout = 500;
        private static int loopTimeoutX = Config.delayMs;
        private static Process prcRunning;
        private static Process prcSIDFitur;

        private const int MOUSEEVENTF_RIGHTDOWN = 0x0008; /* right button down */
        private const int MOUSEEVENTF_RIGHTUP = 0x0010; /* right button up */
        
        #region Definitions/DLL Imports
        /// <summary>
        /// For PInvoke: Contains information about an entry in the Internet cache
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Size = 80)]
        public struct INTERNET_CACHE_ENTRY_INFOA
        {
            [FieldOffset(0)]
            public uint dwStructSize;
            [FieldOffset(4)]
            public IntPtr lpszSourceUrlName;
            [FieldOffset(8)]
            public IntPtr lpszLocalFileName;
            [FieldOffset(12)]
            public uint CacheEntryType;
            [FieldOffset(16)]
            public uint dwUseCount;
            [FieldOffset(20)]
            public uint dwHitRate;
            [FieldOffset(24)]
            public uint dwSizeLow;
            [FieldOffset(28)]
            public uint dwSizeHigh;
            [FieldOffset(32)]
            public System.Runtime.InteropServices.ComTypes.FILETIME LastModifiedTime;
            [FieldOffset(40)]
            public System.Runtime.InteropServices.ComTypes.FILETIME ExpireTime;
            [FieldOffset(48)]
            public System.Runtime.InteropServices.ComTypes.FILETIME LastAccessTime;
            [FieldOffset(56)]
            public System.Runtime.InteropServices.ComTypes.FILETIME LastSyncTime;
            [FieldOffset(64)]
            public IntPtr lpHeaderInfo;
            [FieldOffset(68)]
            public uint dwHeaderInfoSize;
            [FieldOffset(72)]
            public IntPtr lpszFileExtension;
            [FieldOffset(76)]
            public uint dwReserved;
            [FieldOffset(76)]
            public uint dwExemptDelta;
        }

        // For PInvoke: Initiates the enumeration of the cache groups in the Internet cache
        [DllImport(@"wininet",
            SetLastError = true,
            CharSet = CharSet.Auto,
            EntryPoint = "FindFirstUrlCacheGroup",
            CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr FindFirstUrlCacheGroup(
            int dwFlags,
            int dwFilter,
            IntPtr lpSearchCondition,
            int dwSearchCondition,
            ref long lpGroupId,
            IntPtr lpReserved);

        // For PInvoke: Retrieves the next cache group in a cache group enumeration
        [DllImport(@"wininet",
            SetLastError = true,
            CharSet = CharSet.Auto,
            EntryPoint = "FindNextUrlCacheGroup",
            CallingConvention = CallingConvention.StdCall)]
        public static extern bool FindNextUrlCacheGroup(
            IntPtr hFind,
            ref long lpGroupId,
            IntPtr lpReserved);

        // For PInvoke: Releases the specified GROUPID and any associated state in the cache index file
        [DllImport(@"wininet",
            SetLastError = true,
            CharSet = CharSet.Auto,
            EntryPoint = "DeleteUrlCacheGroup",
            CallingConvention = CallingConvention.StdCall)]
        public static extern bool DeleteUrlCacheGroup(
            long GroupId,
            int dwFlags,
            IntPtr lpReserved);

        // For PInvoke: Begins the enumeration of the Internet cache
        [DllImport(@"wininet",
            SetLastError = true,
            CharSet = CharSet.Auto,
            EntryPoint = "FindFirstUrlCacheEntryA",
            CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr FindFirstUrlCacheEntry(
            [MarshalAs(UnmanagedType.LPTStr)] string lpszUrlSearchPattern,
            IntPtr lpFirstCacheEntryInfo,
            ref int lpdwFirstCacheEntryInfoBufferSize);

        // For PInvoke: Retrieves the next entry in the Internet cache
        [DllImport(@"wininet",
            SetLastError = true,
            CharSet = CharSet.Auto,
            EntryPoint = "FindNextUrlCacheEntryA",
            CallingConvention = CallingConvention.StdCall)]
        public static extern bool FindNextUrlCacheEntry(
            IntPtr hFind,
            IntPtr lpNextCacheEntryInfo,
            ref int lpdwNextCacheEntryInfoBufferSize);

        // For PInvoke: Removes the file that is associated with the source name from the cache, if the file exists
        [DllImport(@"wininet",
            SetLastError = true,
            CharSet = CharSet.Auto,
            EntryPoint = "DeleteUrlCacheEntryA",
            CallingConvention = CallingConvention.StdCall)]
        public static extern bool DeleteUrlCacheEntry(
            IntPtr lpszUrlName);

        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);
        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, long dwFlags, long dwExtraInfo);
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        #endregion

        public enum clickTipe : uint
        {
            left = 0,
            right = 1,
            left_double = 2
        }

        #region Process
        /// <summary>
        /// Start Process
        /// </summary>
        /// <param name="processName">Name of process</param>
        /// <param name="processLocation">Location of executetable/process file</param>
        protected internal static void invokeProcess(string processName, string processLocation)
        {
            if (Process.GetProcessesByName(processName).Length < 1)
                prcRunning = Process.Start(processLocation);
            else
            {
                Process[] prcx = Process.GetProcessesByName(processName);
                prcRunning = prcx[0];
                killProcess(processName);
                invokeProcess(processName, processLocation);
            }
        }

        /// <summary>
        /// Stop process
        /// </summary>
        /// <param name="processName">Name of process</param>
        protected internal static void killProcess(string processName)
        {
            prcRunning.CloseMainWindow();
            Process prc;

            if (Process.GetProcessesByName(processName).Length > 0)
            {
                prc = myFunctions.getProcess(processName);
                while (prc != null)
                {
                    prc.Kill();
                    Thread.Sleep(200);
                    prc = myFunctions.getProcess(processName);
                }
            }
        }

        /// <summary>
        /// Get specific process running
        /// </summary>
        /// <param name="processName">Name of process</param>
        /// <returns>running process</returns>
        protected internal static Process getProcess(string processName)
        {
            Process[] p = Process.GetProcessesByName(processName);
            if (p.Length > 0)
                return p[0];
            else
                return null;
        }
        #endregion

        #region Other Function
        /// <summary>
        /// switch focus and bring to front
        /// </summary>
        /// <param name="proc">Name of process</param>
        protected internal static void bringFront(Process proc)
        {
            SetForegroundWindow(proc.MainWindowHandle);
        }

        /// <summary>
        /// Simulates mouse click
        /// </summary>
        /// <param name="tipe">left, right, or double click</param>
        /// <param name="X">X position in pixel</param>
        /// <param name="Y">Y position in pixel</param>
        protected internal static void mouseClick(clickTipe tipe, int X, int Y)
        {
            mouseClick(tipe, X, Y, 50);
        }

        /// <summary>
        /// Simulates mouse click
        /// </summary>
        /// <param name="tipe">left, right, or double click</param>
        /// <param name="X">X position in pixel</param>
        /// <param name="Y">Y position in pixel</param>
        /// <param name="timeOut">delay between onClickDown and onClickUp</param>
        protected internal static void mouseClick(clickTipe tipe, int X, int Y, int timeOut)
        {
            Cursor.Position = new System.Drawing.Point(X, Y);
            Thread.Sleep(20);

            if (tipe == clickTipe.left)
            {
                if(Cursor.Position.X != X && Cursor.Position.Y != Y)
                    Cursor.Position = new System.Drawing.Point(X, Y);
                mouse_event(2, X, Y, 0, 0);
                Thread.Sleep(timeOut);

                if (Cursor.Position.X != X && Cursor.Position.Y != Y)
                    Cursor.Position = new System.Drawing.Point(X, Y);
                mouse_event(4, X, Y, 0, 0);
                Thread.Sleep(timeOut);
            }
            else if (tipe == clickTipe.right)
            {
                mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
                mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
                Thread.Sleep(timeOut);
            }
            else if (tipe == clickTipe.left_double)
            {
                mouse_event(2, X, Y, 0, 0);
                mouse_event(4, X, Y, 0, 0);
                Thread.Sleep(100);
                mouse_event(2, X, Y, 0, 0);
                mouse_event(4, X, Y, 0, 0);
                Thread.Sleep(timeOut);
            }
        }

        /// <summary>
        /// Simulates typing
        /// </summary>
        /// <param name="word">words to write</param>
        protected internal static void kbdWrite(string word)
        {
            int xChar, oriChar;

            // find keys to be shifted [ ~!@#$%^&*()_+{}|:"<>? ]
            int[] shiftedKeys = { 126, 33, 64, 35, 36, 37, 94, 38, 42, 40, 
                                  41, 95, 43, 123, 125, 124, 58, 34, 60, 62, 63 };

            // convert some special keys into their on key code
            Dictionary<int, int> convertedKeys = new Dictionary<int, int>();
            convertedKeys.Add(96, 192);     // `
            convertedKeys.Add(45, 189);     // -
            convertedKeys.Add(61, 187);     // =
            convertedKeys.Add(91, 219);     // [
            convertedKeys.Add(93, 221);     // ]
            convertedKeys.Add(92, 220);     // \
            convertedKeys.Add(59, 186);     // ;
            convertedKeys.Add(39, 222);     // '
            convertedKeys.Add(44, 188);     // ,
            convertedKeys.Add(46, 190);     // .
            convertedKeys.Add(47, 191);     // /

            convertedKeys.Add(126, 192);    // ~
            convertedKeys.Add(33, 49);      // !
            convertedKeys.Add(64, 50);      // @
            convertedKeys.Add(35, 51);      // #
            convertedKeys.Add(36, 52);      // $
            convertedKeys.Add(37, 53);      // %
            convertedKeys.Add(94, 54);      // ^
            convertedKeys.Add(38, 55);      // &
            convertedKeys.Add(42, 56);      // *
            convertedKeys.Add(40, 57);      // (
            convertedKeys.Add(41, 48);      // )
            convertedKeys.Add(95, 189);     // _
            convertedKeys.Add(43, 187);     // +
            convertedKeys.Add(123, 219);    // {
            convertedKeys.Add(125, 221);    // }
            convertedKeys.Add(124, 220);    // |
            convertedKeys.Add(58, 186);     // :
            convertedKeys.Add(34, 222);     // "
            convertedKeys.Add(60, 188);     // <
            convertedKeys.Add(62, 190);     // >
            convertedKeys.Add(63, 191);     // ?


            const int KEYEVENTF_EXTENDEDKEY = 0x0001;   // Key Down
            const int KEYEVENTF_KEYUP = 0x0002;         // Key Up
            const int VK_SHIFT = 0x0010;                 // Shift Key


            for (int i = 0; i < word.Length; i++)
            {
                xChar = Convert.ToChar(word.Substring(i, 1));
                oriChar = xChar;
                if (xChar > 96 && xChar < 122)
                    xChar = xChar - 32;
                if (convertedKeys.ContainsKey(xChar) == true)
                    xChar = convertedKeys[xChar];

                byte KeyCode = Convert.ToByte(xChar);

                //Config.writeLog(oriChar.ToString() + " = " + xChar.ToString());
                //Config.writeLog(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss | ") + word.Substring(i, 1) + " = " + keyTest.ToString());
                if ((oriChar > 64 && oriChar < 91) || shiftedKeys.Contains(oriChar))
                {
                    keybd_event(VK_SHIFT, 0, 0, 0);
                    keybd_event(KeyCode, 0, 0, 0);
                    keybd_event(KeyCode, 0, KEYEVENTF_KEYUP, 0);
                    keybd_event(VK_SHIFT, 0, KEYEVENTF_KEYUP, 0);
                }
                else
                    keybd_event(KeyCode, 0, 0, 0);
                //Application.DoEvents();
            }


        }
        #endregion

        #region Controls Modifier
        protected internal static InvokePattern getInvokePattern(AutomationElement element)
        {
            return element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
        }

        protected internal static SelectionItemPattern getSelectionItemPattern(AutomationElement element)
        {
            return element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
        }

        protected internal static ValuePattern getValuePattern(AutomationElement element)
        {
            return element.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
        }

        protected internal static ExpandCollapsePattern getExpandCollapsePattern(AutomationElement element)
        {
            return element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
        }

        protected internal static WindowPattern getWindowPattern(AutomationElement element)
        {
            return element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
        }

        protected internal static TransformPattern getTransformPattern(AutomationElement element)
        {
            return element.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
        }

        /// <summary>
        /// Search for specific Form based on name
        /// </summary>
        /// <param name="formName">name of the form</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getMainForm(string formName)
        {
            AutomationElement desktop = AutomationElement.RootElement;
            AutomationElement mainForm = null;
            int cnt = 0;

            do
            {
                prcSIDFitur = getProcess("SID_Fitur");
                prcRunning = getProcess("SIDM_Bank");
                if (prcRunning != null)
                {
                    // if SID not responding (usually web authentication), do not find element, causing hang..
                    if (prcRunning.Responding == true && prcSIDFitur == null)
                        mainForm = desktop.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.NameProperty, formName));
                }
                cnt++;
                myFunctions f = new myFunctions();
                f.Delay(loopTimeout);
            }
            while (mainForm == null);

            return mainForm;
        }

        /// <summary>
        /// Search for specific Form based on name, but it's tentative (such as error form)
        /// </summary>
        /// <param name="formName">name of the form</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getMainFormFac(string formName)
        {
            return getMainFormFac(formName, loop, loopTimeoutX);
        }

        /// <summary>
        /// Search for specific Form based on name, but it's tentative (such as error form)
        /// </summary>
        /// <param name="formName">name of the form</param>
        /// <param name="loops">how many times to search</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getMainFormFac(string formName, int loops)
        {
            return getMainFormFac(formName, loops, loopTimeoutX);
        }

        /// <summary>
        /// Search for specific Form based on name, but it's tentative (such as error form)
        /// </summary>
        /// <param name="formName">name of the form</param>
        /// <param name="loops">how many times to search</param>
        /// <param name="timeOuts">delay between loop</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getMainFormFac(string formName, int loops, int timeOuts)
        {
            AutomationElement desktop = AutomationElement.RootElement;
            AutomationElement mainForm = null;
            int cnt = 0;

            do
            {
                prcSIDFitur = getProcess("SID_Fitur");
                prcRunning = getProcess("SIDM_Bank");
                if (prcRunning != null)
                {
                    // if SID not responding (usually web authentication), do not find element, causing hang..
                    if (prcRunning.Responding == true && prcSIDFitur == null)
                        mainForm = desktop.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.NameProperty, formName));
                }
                cnt++;
                myFunctions f = new myFunctions();
                f.Delay(timeOuts);
            }
            while (mainForm == null && cnt < loops);


            return mainForm;
        }

        /// <summary>
        /// Search for specific element based on type and name within the form
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// /// <param name="ctrlIndex">index of the element</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getChildrenControl(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            return getCustomControlAll(mainForm, TreeScope.Children, ctrlType, ctrlName, ctrlIndex);
        }

        /// <summary>
        /// Search for specific element based on type and name within the form, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getChildrenControlFac(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            return getCustomControlAllFac(mainForm, TreeScope.Children, ctrlType, ctrlName, ctrlIndex);
        }

        /// <summary>
        /// Search for specific element based on type and name within the form, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <param name="loops">delay times</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getChildrenControlFac(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex, int loops)
        {
            return getCustomControlAllFac(mainForm, TreeScope.Children, ctrlType, ctrlName, ctrlIndex, loops);
        }

        /// <summary>
        /// Search for specific element based on type and name within the form, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <param name="loops">delay times</param>
        /// <param name="timeOuts">delay timeout</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getChildrenControlFac(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex, int loops, int timeOuts)
        {
            return getCustomControlAllFac(mainForm, TreeScope.Children, ctrlType, ctrlName, ctrlIndex, loops, timeOuts);
        }

        /// <summary>
        /// Search for specific element based on type and name within the form and descendant form
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// /// <param name="ctrlIndex">index of the element</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getDescendantControl(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            return getCustomControlAll(mainForm, TreeScope.Descendants, ctrlType, ctrlName, ctrlIndex);
        }

        /// <summary>
        /// Search for specific element based on type and name within the form and descendant form, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getDescendantControlFac(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            return getCustomControlAllFac(mainForm, TreeScope.Descendants, ctrlType, ctrlName, ctrlIndex);
        }

        /// <summary>
        /// Search for specific element based on type and name within the form and descendant form, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <param name="loops">loop delay</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getDescendantControlFac(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex, int loops)
        {
            return getCustomControlAllFac(mainForm, TreeScope.Descendants, ctrlType, ctrlName, ctrlIndex, loops);
        }

        /// <summary>
        /// Search for specific element based on type and name within the form and descendant form, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <param name="loops">loop delay</param>
        /// <param name="timeOuts">loop timeout</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getDescendantControlFac(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex, int loops, int timeOuts)
        {
            return getCustomControlAllFac(mainForm, TreeScope.Descendants, ctrlType, ctrlName, ctrlIndex, loops, timeOuts);
        }

        /// <summary>
        /// Search for specific element based on type and name within root and descendant form
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// /// <param name="ctrlIndex">index of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getSubtreeControl(AutomationElement mainForm, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            return getCustomControlAll(mainForm, TreeScope.Subtree, ctrlType, ctrlName, ctrlIndex);
        }

        /// <summary>
        /// Search for one specific element based on type and name in scope
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="scope">scope to search</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <returns></returns>
        protected internal static AutomationElement getCustomControlFirst(AutomationElement mainForm, TreeScope scope, ControlType ctrlType, string ctrlName)
        {
            AutomationElement el = mainForm.FindFirst(scope,
                new AndCondition(new PropertyCondition(AutomationElement.ControlTypeProperty, ctrlType),
                    new OrCondition(new PropertyCondition(AutomationElement.NameProperty, ctrlName), new PropertyCondition(AutomationElement.AutomationIdProperty, ctrlName))));

            //testing purpose, looping if element not found
            int cnt = 0;
            while (el == null && cnt < loop)
            {
                AndCondition txtCond = new AndCondition(new PropertyCondition(AutomationElement.NameProperty, ctrlName), new PropertyCondition(AutomationElement.AutomationIdProperty, ctrlName));
                el = mainForm.FindFirst(scope, txtCond);
                cnt++;
                Thread.Sleep(loopTimeout);
            }

            return el;
        }

        /// <summary>
        /// Search for all specific element based on type and name in scope
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="scope">scope to search</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getCustomControlAll(AutomationElement mainForm, TreeScope scope, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            AutomationElement el = null;
            AutomationElementCollection elControls = mainForm.FindAll(scope,
                new PropertyCondition(AutomationElement.ControlTypeProperty, ctrlType));

            foreach (AutomationElement elControl in elControls)
            {
                if (ctrlIndex == 0)
                    continue;
                string id = "", name = "";
                try
                {
                    id = elControl.Current.AutomationId;
                    name = elControl.Current.Name;
                }
                catch { }
                if (id == ctrlName || name == ctrlName)
                {
                    el = elControl;
                    ctrlIndex--;
                }
            }

            while (el == null)
            {
                AndCondition txtCond = new AndCondition(new PropertyCondition(AutomationElement.NameProperty, ctrlName), new PropertyCondition(AutomationElement.AutomationIdProperty, ctrlName));
                el = mainForm.FindFirst(scope, txtCond);
                myFunctions f = new myFunctions();
                f.Delay(loopTimeout);
                //Thread.Sleep(loopTimeout);
            }

            return el;
        }

        /// <summary>
        /// Search for all specific element based on type and name in scope, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="scope">scope to search</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getCustomControlAllFac(AutomationElement mainForm, TreeScope scope, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            return getCustomControlAllFac(mainForm, scope, ctrlType, ctrlName, ctrlIndex, loop, loopTimeoutX);
        }

        /// <summary>
        /// Search for all specific element based on type and name in scope, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="scope">scope to search</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <param name="loops">looping times</param>
        /// <returns>Automation element</returns>
        protected internal static AutomationElement getCustomControlAllFac(AutomationElement mainForm, TreeScope scope, ControlType ctrlType, string ctrlName, int ctrlIndex, int loops)
        {
            return getCustomControlAllFac(mainForm, scope, ctrlType, ctrlName, ctrlIndex, loops, loopTimeoutX);
        }

        /// <summary>
        /// Search for all specific element based on type and name in scope, but it's tentative (such as error form)
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="scope">scope to search</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <param name="loops">looping times</param>
        /// <param name="timeOuts">looping timeout</param>
        /// <returns></returns>
        protected internal static AutomationElement getCustomControlAllFac(AutomationElement mainForm, TreeScope scope, ControlType ctrlType, string ctrlName, int ctrlIndex, int loops, int timeOuts)
        {
            AutomationElement el = null;
            AutomationElementCollection elControls = mainForm.FindAll(scope,
                new PropertyCondition(AutomationElement.ControlTypeProperty, ctrlType));

            foreach (AutomationElement elControl in elControls)
            {
                if (ctrlIndex == 0)
                    continue;
                string id = "", name = "";
                try
                {
                    id = elControl.Current.AutomationId;
                    name = elControl.Current.Name;
                }
                catch { }
                if (id == ctrlName || name == ctrlName)
                {
                    el = elControl;
                    ctrlIndex--;
                }
            }

            int cnt = 0;
            while (el == null && cnt < loops)
            {
                AndCondition txtCond = new AndCondition(new PropertyCondition(AutomationElement.NameProperty, ctrlName), new PropertyCondition(AutomationElement.AutomationIdProperty, ctrlName));
                el = mainForm.FindFirst(scope, txtCond);
                cnt++;
                myFunctions f = new myFunctions();
                f.Delay(timeOuts);
            }

            return el;
        }

        /// <summary>
        /// Search for specific child element
        /// </summary>
        /// <param name="elControl">element to check</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <param name="ctrlIndex">index of the element</param>
        /// <returns>Child of element</returns>
        protected internal static AutomationElement getChildElement(AutomationElement elControl, ControlType ctrlType, string ctrlName, int ctrlIndex)
        {
            AutomationElement elNode = TreeWalker.ContentViewWalker.GetFirstChild(elControl);
            AutomationElement elRet = null;
            int ctrlIndexOri = ctrlIndex;

            ctrlIndex = ctrlIndexOri;

            while (elNode != null && ctrlIndex > 0 && elRet == null)
            {
                string elClass, elName, elAutoID;
                elClass = elNode.Current.LocalizedControlType;
                elName = elNode.Current.Name;
                elAutoID = elNode.Current.AutomationId;

                if (elClass != ctrlType.ToString() && (elName != ctrlName || elAutoID != ctrlName) && ctrlIndex > 1)
                {
                    elNode = TreeWalker.ContentViewWalker.GetNextSibling(elNode);
                    ctrlIndex--;
                }
                else
                {
                    elRet = elNode;
                }
            }

            return elRet;
        }

        /// <summary>
        /// Search for specific deep level menu
        /// </summary>
        /// <param name="mainForm">parent where element resides</param>
        /// <param name="ctrlType">type of the element</param>
        /// <param name="ctrlName">name of the element</param>
        /// <returns></returns>
        protected internal static AutomationElement getDeepMenuControl(AutomationElement mainForm, TreeScope ctrlScope, ControlType ctrlType, string ctrlName)
        {
            AutomationElement el = null;
            AutomationElementCollection elControls = mainForm.FindAll(ctrlScope,
                new PropertyCondition(AutomationElement.ControlTypeProperty, ctrlType));

            foreach (AutomationElement elControl in elControls)
            {
                string id = "", name = "";
                try
                {
                    id = elControl.Current.AutomationId;
                    name = elControl.Current.Name;
                }
                catch { }

                if (id == ctrlName || name == ctrlName)
                {
                    el = elControl;
                }
                else
                {
                    el = el == null ? getDeepMenuControl(elControl, ctrlScope, ControlType.MenuItem, ctrlName) : el;
                }
            }

            return el;
        }

        protected internal static AutomationElementCollection getAllControll(AutomationElement mainForm, TreeScope scope, ControlType ctrlType)
        {
            return mainForm.FindAll(scope, new PropertyCondition(AutomationElement.ControlTypeProperty, ctrlType));
        }

        /// <summary>
        /// Left Click button or Expand menu
        /// </summary>
        /// <param name="elControl">object element</param>        
        protected internal static bool uLeftClick(AutomationElement elControl)
        {
            bool ret = false;
            string elName = "", elType = "";
            try
            {
                elName = elControl.Current.Name;
                elType = elControl.Current.LocalizedControlType;
                getInvokePattern(elControl).Invoke();
                Thread.Sleep(200);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("cannot click " + elName + "|" + elType +
                    ", supported pattern is: " + getSupportedPattern(elControl) + 
                    ", exception message: " + ex.Message, Config.LogType.ERR_APP);

            }

            return ret;
        }

        /// <summary>
        /// Select element
        /// </summary>
        /// <param name="elControl">element name</param>
        /// <returns>success or not</returns>
        protected internal static bool uSelect(AutomationElement elControl)
        {
            bool ret = false;
            try
            {
                getSelectionItemPattern(elControl).Select();
                Thread.Sleep(200);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("cannot select element " + elControl.Current.Name + ", exception message: " + ex.Message, Config.LogType.ERR_APP);
            }

            return ret;
        }

        /// <summary>
        /// Select multiple element(s)
        /// </summary>
        /// <param name="elControls">array of element(s)</param>
        /// <returns></returns>
        protected internal static bool uSelectMulti(AutomationElement[] elControls)
        {
            bool ret = false;
            try
            {
                foreach (AutomationElement elControl in elControls)
                {
                    getSelectionItemPattern(elControl).AddToSelection();
                    Thread.Sleep(500);
                }
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("cannot select element(s), exception message: " + ex.Message, Config.LogType.ERR_APP);
            }
            return ret;
        }

        /// <summary>
        /// Write text into input such as textbox or textarea
        /// </summary>
        /// <param name="elControl">object element</param>
        /// <param name="textValue">object text</param>
        protected internal static bool uWrite(AutomationElement elControl, string textValue)
        {
            bool ret = false;
            try
            {
                getValuePattern(elControl).SetValue(textValue);
                Thread.Sleep(200);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("Cannot type into element, exception message: " + ex.Message, Config.LogType.ERR_APP);
            }

            return ret;
        }

        /// <summary>
        /// Expand menu control
        /// </summary>
        /// <param name="elControl">object element</param>
        /// <returns></returns>
        protected internal static bool uExpand(AutomationElement elControl)
        {
            bool ret = false;
            string elementName = "";
            try
            {
                if (elControl != null)
                    elementName = elControl.Current.Name;
                
                if (getExpandCollapsePattern(elControl).Current.ExpandCollapseState == ExpandCollapseState.Expanded)
                    getExpandCollapsePattern(elControl).Collapse();
                else
                    getExpandCollapsePattern(elControl).Expand();

                Thread.Sleep(500);
                ret = true;
            }
            catch (InvalidOperationException ex)
            {
                ret = false;
                Config.writeLog("Cannot expand element " + elementName + 
                    ", supported pattern is: " + getSupportedPattern(elControl) + 
                    ", exception message: " + ex.Message, Config.LogType.ERR_APP);
            }

            return ret;
        }

        /// <summary>
        /// Resize control
        /// </summary>
        /// <param name="elControl">object element</param>
        /// <param name="height">object height</param>
        /// <param name="width">object width</param>
        /// <returns></returns>
        protected internal static bool uResizeWindow(AutomationElement elControl, double height, double width)
        {
            bool ret = false;
            try
            {
                if (getTransformPattern(elControl).Current.CanResize == true)
                {
                    getTransformPattern(elControl).Resize(width, height);
                }
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("Cannot resize element, exception message: " + ex.Message, Config.LogType.ERR_APP);
            }

            return ret;
        }

        /// <summary>
        /// Maximize window
        /// </summary>
        /// <param name="elControl">window name</param>
        /// <returns>success or not</returns>
        protected internal static bool uMaxWindow(AutomationElement elControl)
        {
            bool ret = false;
            try
            {
                if (getWindowPattern(elControl).Current.WindowVisualState != WindowVisualState.Maximized &&
                    getWindowPattern(elControl).Current.CanMaximize == true)
                    getWindowPattern(elControl).SetWindowVisualState(WindowVisualState.Maximized);

                Thread.Sleep(500);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("Cannot maximize form " + elControl.Current.Name + ", exception message: " + ex.Message, Config.LogType.ERR_APP);
            }

            return ret;
        }

        /// <summary>
        /// Minimize window
        /// </summary>
        /// <param name="elControl">window name</param>
        /// <returns>success or not</returns>
        protected internal static bool uMinWindow(AutomationElement elControl)
        {
            bool ret = false;
            try
            {
                if (getWindowPattern(elControl).Current.WindowVisualState != WindowVisualState.Minimized &&
                    getWindowPattern(elControl).Current.CanMinimize == true)
                    getWindowPattern(elControl).SetWindowVisualState(WindowVisualState.Minimized);

                Thread.Sleep(500);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("Cannot minimize form " + elControl.Current.Name + ", exception message: " + ex.Message, Config.LogType.ERR_APP);
            }

            return ret;
        }

        /// <summary>
        /// Normalize window
        /// </summary>
        /// <param name="elControl">window name</param>
        /// <returns>success or not</returns>
        protected internal static bool uNormalWindow(AutomationElement elControl)
        {
            bool ret = false;
            try
            {
                if (getWindowPattern(elControl).Current.WindowVisualState != WindowVisualState.Normal)
                    getWindowPattern(elControl).SetWindowVisualState(WindowVisualState.Normal);

                Thread.Sleep(500);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                Config.writeLog("Cannot normalize form " + elControl.Current.Name + ", exception message: " + ex.Message, Config.LogType.ERR_APP);
            }

            return ret;
        }

        /// <summary>
        /// Retrieve info about specific element(s) in the form within type
        /// </summary>
        /// <param name="mainForm">parent element or form where element is resides</param>
        /// <param name="ctrlType">type of element to search</param>
        /// <returns>String Info of all element(s)</returns>
        protected internal static string getElementTypeInfo(AutomationElement mainForm, ControlType ctrlType)
        {
            return getElementInfo(mainForm, TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ctrlType));
        }

        /// <summary>
        /// Retrieve info about specific element(s) in the form within scope
        /// </summary>
        /// <param name="mainForm">parent element or form where element is resides</param>
        /// <param name="scope">scope of mainForm</param>
        /// <returns>String Info of all element(s)</returns>
        protected internal static string getElementScopeInfo(AutomationElement mainForm, TreeScope scope)
        {
            return getElementInfo(mainForm, scope, PropertyCondition.TrueCondition);
        }

        /// <summary>
        /// Retrieve info about all type element(s) in the form within scope and condition(s)
        /// </summary>
        /// <param name="mainForm">parent element or form where element is resides</param>
        /// <param name="scope">scope of mainForm</param>
        /// <param name="cond">condition(s) to search</param>
        /// <returns>String Info of all element(s)</returns>
        protected internal static string getElementInfo(AutomationElement mainForm, TreeScope scope, Condition cond)
        {
            StringBuilder sbInfo = new StringBuilder();
            string result = null;
            AutomationElementCollection elCollection = mainForm.FindAll(scope, cond);

            foreach (AutomationElement el in elCollection)
            {
                ControlType ctrl = el.Current.ControlType;
                sbInfo.AppendLine("Element type: " + ctrl.LocalizedControlType + ", Element id: " + el.Current.AutomationId + ", Element name: " + el.Current.Name + ", fw id: " + el.Current.FrameworkId + ", native id: " + el.Current.NativeWindowHandle.ToString());
            }

            result = sbInfo.ToString();
            return result;
        }

        /// <summary>
        /// Retrieve info about pattern(s) that is supported by element
        /// </summary>
        /// <param name="elControl">control to check</param>
        /// <returns>String info of all pattern(s)</returns>
        protected internal static string getSupportedPattern(AutomationElement elControl)
        {
            StringBuilder sbInfo = new StringBuilder();
            string result = null;
            AutomationPattern[] elPatterns = elControl.GetSupportedPatterns();

            foreach (AutomationPattern elPattern in elPatterns)
            {
                sbInfo.AppendLine("Programmatic Name: " + elPattern.ProgrammaticName);
            }

            result = sbInfo.ToString();
            return result;
        }
        
        #endregion

        #region Browser Control
        protected internal static string getBrowserDetailInfo(WebBrowser Wb)
        {
            StringBuilder sbInfo = new StringBuilder();
            string result = null;

            try
            {
                HTMLDocument objDoc = (HTMLDocument)Wb.Document.DomDocument;
                IHTMLElementCollection wbInput = objDoc.getElementsByTagName("input");
                string strName = null;
                string strType = null;

                foreach (IHTMLElement wbelm in wbInput)
                {
                    try
                    {
                        strName = wbelm.getAttribute("name", 2).ToString();
                        strType = wbelm.getAttribute("type", 2).ToString();
                    }
                    catch { }
                    sbInfo.AppendLine("Input name: " + strName + ", Input type: " + strType);
                }
            }
            catch { }

            result = sbInfo.ToString();
            return result;
        }

        protected internal static bool webInputText(WebBrowser Wb, string inputName, string inputText)
        {
            bool success = false;
            try
            {
                HTMLDocument objDoc = (HTMLDocument)Wb.Document.DomDocument;
                IHTMLElement wbInput = objDoc.getElementById(inputName);

                wbInput.setAttribute("value", inputText, 2);
                Thread.Sleep(500);
                success = true;
            }
            catch (Exception e)
            {
                success = false;
                Config.writeLog("cannot write into " + inputName + ", exception message: " + e.Message, Config.LogType.ERR_WB);
            }
            return success;
        }

        protected internal static bool webSubmitForm(WebBrowser Wb, string inputName)
        {
            bool success = false;
            try
            {
                HTMLDocument objDoc = (HTMLDocument)Wb.Document.DomDocument;
                IHTMLElement wbInput = objDoc.getElementById(inputName);
                string strName = wbInput.getAttribute("name", 2).ToString();
                string strType = wbInput.getAttribute("type", 2).ToString();

                if (strName.ToLower().Trim() == inputName.ToLower().Trim() && strType.ToLower().Trim() == "submit")
                    wbInput.click();

                //Thread.Sleep(1000);
                success = true;
            }
            catch (Exception e)
            {
                success = false;
                //Config.writeLog(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " |WEB ERROR| cannot submit " + inputName + ", exception message: " + e.Message);
            }
            return success;
        }

        protected internal static void webClickTampilkan(WebBrowser Wb)
        {
            HTMLDocument objDoc = (HTMLDocument)Wb.Document.DomDocument;
            IHTMLElementCollection wbInput = objDoc.getElementsByTagName("input");
            string strName = null;
            string strType = null;

            foreach (IHTMLElement wbElm in wbInput)
            {
                try
                {
                    strName = wbElm.getAttribute("name", 2).ToString();
                    strType = wbElm.getAttribute("type", 2).ToString();
                }
                catch { }
                if (strType.ToLower().Trim() == "submit")
                {
                    if (strName.ToLower().Trim() == "sbt1")
                    {
                        wbElm.click();
                    }
                }
            }
        }

        protected internal static bool getDownloadLink(WebBrowser Wb)
        {
            bool success = false;

            return success;
        }

        protected internal static void ClearCache()
        {
            // Indicates that all of the cache groups in the user's system should be enumerated
            const int CACHEGROUP_SEARCH_ALL = 0x0;
            // Indicates that all the cache entries that are associated with the cache group
            // should be deleted, unless the entry belongs to another cache group.
            const int CACHEGROUP_FLAG_FLUSHURL_ONDELETE = 0x2;
            // File not found.
            const int ERROR_FILE_NOT_FOUND = 0x2;
            // No more items have been found.
            const int ERROR_NO_MORE_ITEMS = 259;
            // Pointer to a GROUPID variable
            long groupId = 0;

            // Local variables
            int cacheEntryInfoBufferSizeInitial = 0;
            int cacheEntryInfoBufferSize = 0;
            IntPtr cacheEntryInfoBuffer = IntPtr.Zero;
            INTERNET_CACHE_ENTRY_INFOA internetCacheEntry;
            IntPtr enumHandle = IntPtr.Zero;
            bool returnValue = false;

            // Delete the groups first.
            // Groups may not always exist on the system.
            // For more information, visit the following Microsoft Web site:
            // http://msdn.microsoft.com/library/?url=/workshop/networking/wininet/overview/cache.asp            
            // By default, a URL does not belong to any group. Therefore, that cache may become
            // empty even when the CacheGroup APIs are not used because the existing URL does not belong to any group.            
            enumHandle = FindFirstUrlCacheGroup(0, CACHEGROUP_SEARCH_ALL, IntPtr.Zero, 0, ref groupId, IntPtr.Zero);
            // If there are no items in the Cache, you are finished.
            if (enumHandle == IntPtr.Zero && ERROR_NO_MORE_ITEMS == Marshal.GetLastWin32Error())
                return;

            // solution to prevent DeleteUrlCacheGroup keep deleting same groupID, http://stackoverflow.com/questions/11690121/wininet-cache-api-hangs-in-windows-8
            long prevGroupId = 0;

            // Loop through Cache Group, and then delete entries.
            while (true)
            {
                if (ERROR_NO_MORE_ITEMS == Marshal.GetLastWin32Error() || ERROR_FILE_NOT_FOUND == Marshal.GetLastWin32Error()) { break; }

                // solution to prevent DeleteUrlCacheGroup keep deleting same groupID
                if (prevGroupId == groupId) break;

                // Delete a particular Cache Group.
                returnValue = DeleteUrlCacheGroup(groupId, CACHEGROUP_FLAG_FLUSHURL_ONDELETE, IntPtr.Zero);
                if (!returnValue && ERROR_FILE_NOT_FOUND == Marshal.GetLastWin32Error())
                {
                    returnValue = FindNextUrlCacheGroup(enumHandle, ref groupId, IntPtr.Zero);
                }

                if (!returnValue && (ERROR_NO_MORE_ITEMS == Marshal.GetLastWin32Error() || ERROR_FILE_NOT_FOUND == Marshal.GetLastWin32Error()))
                    break;

                prevGroupId = groupId;
            }

            // Start to delete URLs that do not belong to any group.
            enumHandle = FindFirstUrlCacheEntry(null, IntPtr.Zero, ref cacheEntryInfoBufferSizeInitial);
            if (enumHandle != IntPtr.Zero && ERROR_NO_MORE_ITEMS == Marshal.GetLastWin32Error())
                return;

            cacheEntryInfoBufferSize = cacheEntryInfoBufferSizeInitial;
            cacheEntryInfoBuffer = Marshal.AllocHGlobal(cacheEntryInfoBufferSize);
            enumHandle = FindFirstUrlCacheEntry(null, cacheEntryInfoBuffer, ref cacheEntryInfoBufferSizeInitial);

            try
            {
                while (true)
                {
                    internetCacheEntry = (INTERNET_CACHE_ENTRY_INFOA)Marshal.PtrToStructure(cacheEntryInfoBuffer, typeof(INTERNET_CACHE_ENTRY_INFOA));
                    if (ERROR_NO_MORE_ITEMS == Marshal.GetLastWin32Error()) { break; }

                    cacheEntryInfoBufferSizeInitial = cacheEntryInfoBufferSize;
                    //returnValue = DeleteUrlCacheEntry(internetCacheEntry.lpszLocalFileName);
                    returnValue = DeleteUrlCacheEntry(internetCacheEntry.lpszSourceUrlName);
                    if (!returnValue)
                    {
                        returnValue = FindNextUrlCacheEntry(enumHandle, cacheEntryInfoBuffer, ref cacheEntryInfoBufferSizeInitial);
                    }
                    if (!returnValue && ERROR_NO_MORE_ITEMS == Marshal.GetLastWin32Error())
                    {
                        break;
                    }
                    if (!returnValue && cacheEntryInfoBufferSizeInitial > cacheEntryInfoBufferSize)
                    {
                        cacheEntryInfoBufferSize = cacheEntryInfoBufferSizeInitial;
                        cacheEntryInfoBuffer = Marshal.ReAllocHGlobal(cacheEntryInfoBuffer, (IntPtr)cacheEntryInfoBufferSize);
                        returnValue = FindNextUrlCacheEntry(enumHandle, cacheEntryInfoBuffer, ref cacheEntryInfoBufferSizeInitial);
                    }
                }
            }
            catch { }
            
            Marshal.FreeHGlobal(cacheEntryInfoBuffer);
        }
        #endregion

        /// <summary>
        /// pause in milisecond(s)
        /// </summary>
        /// <param name="waitTime">pause period in milisecond</param>
        private void Delay(int waitTime)
        {
            int theTime = Environment.TickCount + waitTime;
            while (Environment.TickCount < theTime)
            {
                Application.DoEvents();
                Thread.Sleep(1);
            }
        }
    }

    class iniFile
    {
        public string Path;
        static string EXE = Assembly.GetExecutingAssembly().GetName().Name;

        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string section, string key, string dflt, StringBuilder retVal, int size, string filePath);

        public iniFile(string iniPath)
        {
            Path = new FileInfo(iniPath != null ? iniPath : EXE + ".ini").FullName.ToString();
        }

        public string Read(string key, string section)
        {
            StringBuilder retVal = new StringBuilder(255);
            GetPrivateProfileString(section != null ? section : EXE, key, "", retVal, 255, Path);
            return retVal.ToString();
        }

        public bool keyExists(string key, string section)
        {
            return Read(key, section).Length > 0;
        }
    }

    class XcelEditor
    {
        public enum CellInsert : uint
        {
            rowThenColumn = 0,
            columnThenRow = 1
        }

        public enum CellShift : uint
        {
            row = 0,
            column = 1
        }

        #region EPPlus
        private ExcelPackage EPPackage;
        private ExcelWorksheet EPWorkSheet;
        private bool final = true;

        public void CreateXcel2007(FileInfo file)
        {
            CreateXcel2007(new DirectoryInfo(file.DirectoryName), file.Name);
        }

        /// <summary>
        /// Create new excel file from scratch
        /// </summary>
        /// <param name="di">new excel save directory</param>
        /// <param name="filename">new excel filename</param>
        public void CreateXcel2007(DirectoryInfo di, string filename)
        {
            if (final == true)
            {
                if (File.Exists(di.FullName + @filename))
                    File.Delete(di.FullName + @filename);

                FileInfo fi = new FileInfo(di.FullName + @filename);

                if (fi.Extension == ".xlsx")
                {
                    EPPackage = new ExcelPackage(fi);
                    final = false;
                }
                else
                    throw new Exception("format must be .xlsx");
            }
            else
                throw new Exception("Previous Excel still in progress!!");
        }

        /// <summary>
        /// Create new excel file from template
        /// </summary>
        /// <param name="di">new excel save directory</param>
        /// <param name="filename">new excel filename</param>
        /// <param name="template">template excel</param>
        public void CreateXcel2007Tpl(DirectoryInfo di, string filename, FileInfo template)
        {
            if (final == true)
            {
                if (File.Exists(di.FullName + @filename))
                    File.Delete(di.FullName + @filename);

                FileInfo fi = new FileInfo(di.FullName + @filename);

                if (fi.Extension == ".xlsx")
                {
                    EPPackage = new ExcelPackage(fi, template);
                    final = false;
                }
                else
                    throw new Exception("format must be .xlsx");
            }
            else
                throw new Exception("Previous Excel still in progress!!");
        }

        /// <summary>
        /// Fill value to excel cell based on address
        /// </summary>
        /// <param name="sheetName">Excel sheet name</param>
        /// <param name="address">Excel cell address</param>
        /// <param name="value">Value to fill</param>
        public void SetValueXcel2007(string sheetName, string address, string value)
        {
            if (EPPackage.Workbook.Worksheets[sheetName] == null)
                EPPackage.Workbook.Worksheets.Add(sheetName);

            EPPackage.Workbook.Worksheets[sheetName].Cells[address].Value = value;
        }

        /// <summary>
        /// Fill value to excel cell based on column and row
        /// </summary>
        /// <param name="sheetName">Excel sheet name</param>
        /// <param name="row">Excel row address</param>
        /// <param name="col">Excel column address</param>
        /// <param name="value">Value to fill</param>
        public void SetValueXcel2007(string sheetName, int row, int col, string value)
        {
            if (EPPackage.Workbook.Worksheets[sheetName] == null)
                EPPackage.Workbook.Worksheets.Add(sheetName);

            EPPackage.Workbook.Worksheets[sheetName].Cells[row, col].Value = value;
        }

        public void SetValueXcel2007byName(string sheetName, string rngName, string[] value)
        {
            SetValueXcel2007byName(sheetName, rngName, value, CellShift.row);
        }

        public void SetValueXcel2007byName(string sheetName, string rngName, string[] value, CellShift shiftType)
        {
            SetValueXcel2007byName(sheetName, rngName, value, CellShift.row, CellInsert.rowThenColumn);
        }

        public void SetValueXcel2007byName(string sheetName, string rngName, string[] value, CellShift shiftType, CellInsert insertType)
        {
            if (EPPackage.Workbook.Worksheets[sheetName] == null)
                EPPackage.Workbook.Worksheets.Add(sheetName);

            ExcelWorksheet xSheet = EPPackage.Workbook.Worksheets[sheetName];
            ExcelNamedRange xNamedRange = EPPackage.Workbook.Names[rngName];
            int row = xNamedRange.Start.Row;
            int col = xNamedRange.Start.Column;
            int shiftTimes = 1;

            string[] rngnames = rngName.Split('.');
            if (rngnames.Length >= 2)
                if (rngnames[1] != "" && rngnames[1] != null)
                    shiftType = rngnames[1].ToLower() == "col" ? CellShift.column : CellShift.row;

            if (rngnames.Length >= 3)
                if (rngnames[2] != "" && rngnames[2] != null && int.TryParse(rngnames[2], out shiftTimes) == true)
                    shiftTimes = shiftTimes;

            // check if current cell is still empty, else insert new row or column
            if (xNamedRange.Value != "" && xNamedRange.Value != null)
            {
                if (shiftType == CellShift.column)
                {
                    xSheet.InsertColumn(col + xNamedRange.Columns, shiftTimes);
                    col += shiftTimes;
                }
                else if (shiftType == CellShift.row)
                {
                    xSheet.InsertRow(xNamedRange.Rows + xNamedRange.Rows, shiftTimes);
                    row += shiftTimes;
                }
            }

            xSheet.Cells[row, col].Value = value.ToString();
        }

        public void AutofitExcelSheet(string sheetName)
        {
            EPWorkSheet = EPPackage.Workbook.Worksheets[sheetName];
            EPWorkSheet.Cells[EPWorkSheet.Dimension.Address].AutoFitColumns();
        }

        public void SaveXcel2007(bool finalize)
        {
            EPPackage.Save();
            if (finalize)
                final = true;
        }
        #endregion
    }
}
